package jibe.android.streaming.sr;

import com.google.common.base.Optional;
import jibe.sr.api.SRApi;
import jibe.sr.api.json.SRChannel;
import jibe.sr.api.json.SRPlayList;
import jibe.sr.api.json.SRSchedule;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 *
 */
public class SRApiTest
{
    @Test(enabled = true)
    public void testGetChannel() throws Exception
    {
        Optional<SRChannel> channelById = SRApi.getChannel(132);

        assertNotNull(channelById);
        assertTrue(channelById.isPresent());

        SRChannel srChannel = channelById.get();

        assertSRChannel(srChannel, 132);
    }

    @Test(enabled = true)
    public void testGetChannelEpisode() throws Exception
    {
        List<SRSchedule> schedules = SRApi.getChannelsEpisodes(132);

        assertNotNull(schedules);
        assertFalse(schedules.isEmpty());

        for (SRSchedule schedule : schedules)
        {
            assertSRSchedule(schedule, 132);
        }
    }

    @Test(enabled = true)
    public void testGetChannelRightNowCurrent() throws Exception
    {
        Optional<SRSchedule> rightNow = SRApi.getCurrentScheduledEpisode(132);

        assertNotNull(rightNow);
        assertTrue(rightNow.isPresent());
        assertSRSchedule(rightNow.get(), 132);
    }

    @Test(enabled = true)
    public void testGetChannelRightNowNext() throws Exception
    {
        Optional<SRSchedule> rightNow = SRApi.getNextScheduledEpisode(132);

        assertNotNull(rightNow);
        assertTrue(rightNow.isPresent());
        assertSRSchedule(rightNow.get(), 132);
    }

    @Test(enabled = true)
    public void testGetChannelRightNowSong() throws Exception
    {
        Optional<SRPlayList> playlist = SRApi.getPlaylist(164);

        assertNotNull(playlist);
        assertTrue(playlist.isPresent());
        SRPlayList srPlayList = playlist.get();
        System.out.println(srPlayList.getCurrentSong());
        System.out.println(srPlayList.getNextSong());
    }

    private void assertSRChannel(SRChannel srChannel, int channelId)
    {
        System.out.println("assertSRChannel: " + channelId);

        assertEquals(srChannel.getId(), channelId);
        assertNotNull(srChannel.getName());
        assertNotNull(srChannel.getImage());
        assertNotNull(srChannel.getSite());
        assertNotNull(srChannel.getColor());
        assertNotNull(srChannel.getSchedule());
        assertNotNull(srChannel.getTagline());

        assertTrue(srChannel.getLiveUrlPoorQuality().isPresent());
        assertTrue(srChannel.getLiveUrlLowQuality().isPresent());
        assertTrue(srChannel.getLiveUrlNormalQuality().isPresent());
        assertTrue(srChannel.getLiveUrlHighQuality().isPresent());
    }

    private void assertSRSchedule(SRSchedule schedule, int channelId)
    {
        assertNotNull(schedule.getTitle());
        assertNotNull(schedule.getDescription());
        assertNotNull(schedule.getStartTime(), schedule.toString());
        assertNotNull(schedule.getEndTime());
    }
}
