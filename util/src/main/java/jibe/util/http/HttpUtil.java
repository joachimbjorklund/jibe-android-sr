package jibe.util.http;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import com.squareup.okhttp.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class HttpUtil
{
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtil.class.getSimpleName());

    private HttpUtil()
    {

    }

    public static Builder builder(URL url)
    {
        return new Builder(url);
    }

    public static Optional<String> executeRequestToResponseString(String url)
    {
        LOGGER.info("executeRequest: " + url);
        Optional<HttpURLConnection> urlConnection = openConnection(url, 200);
        if (!urlConnection.isPresent())
        {
            return Optional.absent();
        }
        return responseToString(urlConnection.get());
    }

    public static OkHttpClient newHttpClient()
    {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(2, TimeUnit.SECONDS);
        return client;
    }

    public static Optional<HttpURLConnection> openConnection(String url)
    {
        LOGGER.info("openConnection: " + url);
        try
        {
            return Optional.of(HttpUtil.newHttpClient().open(new URL(url)));
        }
        catch (Exception e)
        {
            LOGGER.error(e.getMessage(), e);
            return Optional.absent();
        }
    }

    public static Optional<HttpURLConnection> openConnection(String url, int expectedStatus)
    {
        try
        {
            Optional<HttpURLConnection> httpURLConnectionOptional = openConnection(url);
            if (!httpURLConnectionOptional.isPresent())
            {
                return Optional.absent();
            }

            HttpURLConnection connection = httpURLConnectionOptional.get();

            int responseCode = connection.getResponseCode();
            if (responseCode != expectedStatus)
            {
                LOGGER.info(String.format("%d != %d", responseCode, expectedStatus));
                return Optional.absent();
            }

            return Optional.of(connection);
        }
        catch (Exception e)
        {
            LOGGER.error(Strings.nullToEmpty(e.getMessage()), e);
            return Optional.absent();
        }
    }

    public static Optional<String> responseToString(HttpURLConnection connection)
    {
        try
        {
            InputStream inputStream = connection.getErrorStream();
            if (inputStream == null)
            {
                inputStream = connection.getInputStream();
            }
            return Optional.of(CharStreams.toString(new InputStreamReader(inputStream, "UTF-8")));
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage(), e);
        }
        return Optional.absent();
    }

    public static class Builder
    {
        private final Map<String, String> requestParams = new HashMap<>();
        private Optional<URL> url = Optional.absent();
        private String requestMethod;
        private Optional<String> cookie = Optional.absent();
        private Optional<String> body = Optional.absent();
        private Optional<String> contentType = Optional.absent();

        public Builder(URL url)
        {
            this.url = Optional.fromNullable(url);
            this.requestMethod = "GET";
        }

        public Builder addRequestParameter(String param, String value)
        {
            requestParams.put(param, value);
            return this;
        }

        public Builder body(String body)
        {
            this.body = Optional.fromNullable(Strings.emptyToNull(body));
            return this;
        }

        public Optional<HttpURLConnection> build()
        {
            if (!url.isPresent())
            {
                LOGGER.error("no url");
                return Optional.absent();
            }

            HttpURLConnection connection = HttpUtil.newHttpClient().open(url.get());

            try
            {
                connection.setRequestMethod(requestMethod);
            }
            catch (ProtocolException e)
            {
                LOGGER.error(e.getMessage(), e);
                return Optional.absent();
            }

            if (cookie.isPresent())
            {
                connection.setRequestProperty("Cookie", cookie.get());
            }

            if (contentType.isPresent())
            {
                connection.setRequestProperty("Content-type", contentType.get());
            }

            if (!requestParams.isEmpty() && !body.isPresent())
            {
                body = Optional.of(getRequestParamsAsString(requestParams));
            }

            if (body.isPresent())
            {
                Optional<HttpURLConnection> optionalConn = writeToOutput(connection, body.get());

                if (!optionalConn.isPresent())
                {
                    LOGGER.error("could not write body");
                    return Optional.absent();
                }

                connection = optionalConn.get();
            }

            return Optional.of(connection);
        }

        public Builder contentType(String contentType)
        {
            this.contentType = Optional.fromNullable(contentType);
            return this;
        }

        public Builder cookie(String cookie)
        {
            this.cookie = Optional.fromNullable(Strings.emptyToNull(cookie));
            return this;
        }

        public Builder forPOST()
        {
            requestMethod = "POST";
            return this;
        }

        public Optional<String> responseToString()
        {
            Optional<HttpURLConnection> connection = build();
            if (!connection.isPresent())
            {
                LOGGER.info("no connection");
                return Optional.absent();
            }
            return HttpUtil.responseToString(connection.get());
        }

        private String getRequestParamsAsString(Map<String, String> requestParams)
        {
            StringBuilder buf = new StringBuilder();
            for (String key : requestParams.keySet())
            {
                buf.append((buf.length() == 0) ? "" : "&").append(key).append("=").append(requestParams.get(key));
            }
            return buf.toString();
        }

        private Optional<HttpURLConnection> writeToOutput(HttpURLConnection connection, String body)
        {
            connection.setDoOutput(true);
            try
            {
                OutputStream outputStream = connection.getOutputStream();
                ByteStreams.copy(new ByteArrayInputStream(body.getBytes()), outputStream);
                outputStream.flush();
                outputStream.close();
            }
            catch (IOException e)
            {
                LOGGER.error(e.getMessage(), e);
                return Optional.absent();
            }

            return Optional.of(connection);
        }
    }
}
