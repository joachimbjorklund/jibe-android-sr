package jibe.util.json;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 *
 */
public class JsonParser
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonParser.class.getSimpleName());

    public static <T> T parse(String jsonData, Class<?>... type)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new GuavaModule());
        objectMapper.registerModule(new JodaModule());

        return parse(jsonData, Lists.newArrayList(type), objectMapper);
    }

    private static <T> T parse(String jsonData, List<Class<?>> types, ObjectMapper objectMapper)
    {
        JavaType javaType = null;
        if (types.isEmpty())
        {
            javaType = objectMapper.getTypeFactory().constructType(Object.class);
        }
        else if (types.size() == 1)
        {
            javaType = objectMapper.getTypeFactory().constructType(types.get(0));
        }
        else if (types.size() > 1)
        {
            ArrayList<Class<?>> genericParameterList = newArrayList(types);
            genericParameterList.remove(0);
            Class<?>[] genericParameters = genericParameterList.toArray(new Class<?>[genericParameterList.size()]);
            javaType = objectMapper.getTypeFactory().constructParametricType(types.get(0), genericParameters);
        }
        //TODO: Explicit Type argument below needed for release server to compile
        try
        {
            return objectMapper.readValue(jsonData, javaType);
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage());
        }

        return null;
    }
}
