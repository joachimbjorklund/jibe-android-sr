package jibe.util.datetime;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;
import java.util.TimeZone;

/**
 *
 */
public class DateTimeUtil
{
    public static DateTimeFormatter getLongDateFormatter()
    {
        return DateTimeFormat.longDate().withLocale(Locale.getDefault());
    }

    public static DateTimeFormatter getLongTimeFormatter()
    {
        return DateTimeFormat.longTime().withLocale(Locale.getDefault());
    }

    public static DateTimeFormatter getMediumDateFormatter()
    {
        return DateTimeFormat.mediumDate().withLocale(Locale.getDefault());
    }

    public static DateTimeFormatter getMediumTimeFormatter()
    {
        return DateTimeFormat.mediumTime().withLocale(Locale.getDefault());
    }

    public static DateTimeFormatter getShortDateFormatter()
    {
        return DateTimeFormat.shortDate().withLocale(Locale.getDefault());
    }

    public static DateTimeFormatter getShortTimeFormatter()
    {
        return DateTimeFormat.shortTime().withLocale(Locale.getDefault());
    }

    public static DateTimeZone getTimeZone()
    {
        return DateTimeZone.forTimeZone(TimeZone.getDefault());
    }

    public static String printLongDate(DateTime date)
    {
        return getLongDateFormatter().print(date.withZone(getTimeZone()));
    }

    public static String printLongTime(DateTime time)
    {
        return getLongTimeFormatter().print(time.withZone(getTimeZone()));
    }

    public static String printMediumDate(DateTime date)
    {
        return getMediumDateFormatter().print(date.withZone(getTimeZone()));
    }

    public static String printMediumTime(DateTime time)
    {
        return getMediumTimeFormatter().print(time.withZone(getTimeZone()));
    }

    public static String printShortDate(DateTime date)
    {
        return getShortDateFormatter().print(date.withZone(getTimeZone()));
    }

    public static String printShortTime(DateTime time)
    {
        return getShortTimeFormatter().print(time.withZone(getTimeZone()));
    }

}
