package jibe.sr;

import com.google.common.base.Optional;
import jibe.sr.api.json.SRChannel;
import jibe.sr.api.json.SRSchedule;
import jibe.util.datetime.DateTimeUtil;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class RightNowProgramInfoBuilder
{
    private static final Logger LOGGER = LoggerFactory.getLogger(RightNowProgramInfoBuilder.class.getSimpleName());

    private static final String TEXT_LEFT_P = "(";
    private static final String TEXT_RIGHT_P = ")";
    private static final String TEXT_SPACE = " ";
    private static final String TEXT_COMMA_SPACE = ", ";
    private static final String TEXT_SPACE_COMMA_SPACE = " , ";
    private static final String TEXT_SPACE_BAR = " |";

    private RightNowProgramInfoBuilder()
    {
        // empty
    }

    public static String buildRightNowInfoText(SRChannel channel, Format format)
    {
        return buildRightNowInfoTextCurrent(channel, format)
                .append(buildRightNowInfoTextNext(channel, format))
                .toString();
    }

    public static StringBuilder buildRightNowInfoTextCurrent(SRChannel channel, Format format)
    {
        StringBuilder sb = new StringBuilder();

        Optional<SRSchedule> episode = channel.getCurrentScheduledEpisode();
        if (!episode.isPresent())
        {
            return sb;
        }

        SRSchedule srSchedule = episode.get();

        String title = srSchedule.getTitle();
        sb.append(title);

        Optional<String> subTitle = srSchedule.getSubTitle();
        if (subTitle.isPresent())
        {
            sb.append(TEXT_SPACE).append(subTitle.get());
        }

        if (format == Format.COMPACT)
        {
            return sb;
        }

        Optional<String> description = srSchedule.getDescription();
        if (description.isPresent())
        {
            sb.append(TEXT_SPACE_COMMA_SPACE).append(description.get());
        }

        if (!channel.getNextScheduledEpisode().isPresent())
        {
            Optional<DateTime> endTime = srSchedule.getStartTime();
            if (endTime.isPresent())
            {
                sb.append(TEXT_SPACE)
                        .append(TEXT_LEFT_P)
                        .append(DateTimeUtil.printShortTime(endTime.get()))
                        .append(TEXT_RIGHT_P)
                        .append(TEXT_SPACE);
            }
        }
        return sb;
    }

    public static StringBuilder buildRightNowInfoTextNext(SRChannel channel, Format format)
    {
        StringBuilder sb = new StringBuilder();

        Optional<SRSchedule> nextEpisodeOptional = channel.getNextScheduledEpisode();
        if (!nextEpisodeOptional.isPresent())
        {
            return sb;
        }

        Optional<SRSchedule> currEpisode = channel.getCurrentScheduledEpisode();
        SRSchedule nextEpisode = nextEpisodeOptional.get();
        if (format != Format.COMPACT)
        {
            if (currEpisode.isPresent())
            {
                sb.append(TEXT_SPACE_BAR);
            }

            Optional<DateTime> startTime = nextEpisode.getStartTime();
            if (startTime.isPresent())
            {
                sb.append(TEXT_SPACE)
                        .append(TEXT_LEFT_P)
                        .append(DateTimeUtil.printShortTime(startTime.get()))
                        .append(TEXT_RIGHT_P);
            }
        }

        String title = nextEpisode.getTitle();
        sb.append(TEXT_SPACE).append(title);

        Optional<String> subTitle = nextEpisode.getSubTitle();
        if (subTitle.isPresent())
        {
            sb.append(TEXT_SPACE)
                    .append(subTitle.get());
        }

        if (format == Format.COMPACT)
        {
            return sb;
        }

        Optional<String> description = nextEpisode.getDescription();
        if (description.isPresent())
        {
            sb.append(TEXT_COMMA_SPACE)
                    .append(description.get());
        }

        return sb;
    }

    public enum Format
    {
        FULL,
        COMPACT;
    }

}
