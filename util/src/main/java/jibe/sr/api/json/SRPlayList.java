package jibe.sr.api.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Optional;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

/**
 *
 */
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SRPlayList
{
    private Optional<SRSong> song = Optional.absent();
    private Optional<SRSong> nextsong = Optional.absent();

    public Optional<SRSong> getCurrentSong()
    {
        return song;
    }

    public Optional<SRSong> getNextSong()
    {
        return nextsong;
    }
}
