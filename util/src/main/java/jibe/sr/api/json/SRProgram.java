package jibe.sr.api.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Optional;

import java.net.URL;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

/**
 *
 */
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SRProgram
{
    private int id;
    private Optional<String> name;
    private Optional<String> description;
    private Optional<String> email;

    private Optional<URL> programimage = Optional.absent();

    public Optional<String> getDescription()
    {
        return description;
    }

    public Optional<String> getEmail()
    {
        return email;
    }

    public int getId()
    {
        return id;
    }

    public Optional<String> getName()
    {
        return name;
    }

    public Optional<URL> getProgramImage()
    {
        return programimage;
    }

    public void setProgramImage(Optional<URL> programImage)
    {
        this.programimage = programImage;
    }
}
