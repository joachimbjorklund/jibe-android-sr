package jibe.sr.api.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Optional;

import java.net.URL;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

/**
 *
 */
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SRChannel
{
    private int id;
    private Optional<String> name = Optional.absent();

    private Optional<String> tagline = Optional.absent();
    private Optional<URL> image = Optional.absent();
    private Optional<URL> siteurl = Optional.absent();

    private Optional<String> color = Optional.absent();
    private Optional<URL> scheduleurl = Optional.absent();

    private Optional<SRSchedule> currentscheduledepisode = Optional.absent();
    private Optional<SRSchedule> nextscheduledepisode = Optional.absent();

    private Optional<URL> liveUrlPoorQuality = Optional.absent();
    private Optional<URL> liveUrlLowQuality = Optional.absent();
    private Optional<URL> liveUrlNormalQuality = Optional.absent();
    private Optional<URL> liveUrlHighQuality = Optional.absent();
    private Optional<SRPlayList> playList;

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        SRChannel channel = (SRChannel) o;

        if (id != channel.id)
        {
            return false;
        }

        return true;
    }

    public Optional<String> getColor()
    {
        return color;
    }

    public Optional<SRSchedule> getCurrentScheduledEpisode()
    {
        return currentscheduledepisode;
    }

    public void setCurrentScheduledEpisode(Optional<SRSchedule> currentScheduledEpisode)
    {
        this.currentscheduledepisode = currentScheduledEpisode;
    }

    public int getId()
    {
        return id;
    }

    public Optional<URL> getImage()
    {
        return image;
    }

    public Optional<URL> getLiveUrlHighQuality()
    {
        return liveUrlHighQuality;
    }

    public void setLiveUrlHighQuality(Optional<URL> liveUrlHighQuality)
    {
        this.liveUrlHighQuality = liveUrlHighQuality;
    }

    public Optional<URL> getLiveUrlLowQuality()
    {
        return liveUrlLowQuality;
    }

    public void setLiveUrlLowQuality(Optional<URL> liveUrlLowQuality)
    {
        this.liveUrlLowQuality = liveUrlLowQuality;
    }

    public Optional<URL> getLiveUrlNormalQuality()
    {
        return liveUrlNormalQuality;
    }

    public void setLiveUrlNormalQuality(Optional<URL> liveUrlNormalQuality)
    {
        this.liveUrlNormalQuality = liveUrlNormalQuality;
    }

    public Optional<URL> getLiveUrlPoorQuality()
    {
        return liveUrlPoorQuality;
    }

    public void setLiveUrlPoorQuality(Optional<URL> liveUrlPoorQuality)
    {
        this.liveUrlPoorQuality = liveUrlPoorQuality;
    }

    public Optional<String> getName()
    {
        return name;
    }

    public Optional<SRSchedule> getNextScheduledEpisode()
    {
        return nextscheduledepisode;
    }

    public void setNextScheduledEpisode(Optional<SRSchedule> nextScheduledEpisode)
    {
        this.nextscheduledepisode = nextScheduledEpisode;
    }

    public Optional<URL> getSchedule()
    {
        return scheduleurl;
    }

    public Optional<URL> getSite()
    {
        return siteurl;
    }

    public Optional<String> getTagline()
    {
        return tagline;
    }

    @Override
    public int hashCode()
    {
        return id;
    }

    public void setPlayList(Optional<SRPlayList> playList)
    {
        this.playList = playList;
    }

    @Override
    public String toString()
    {
        return "SRChannel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tagline=" + tagline +
                ", image=" + image +
                ", siteurl=" + siteurl +
                ", color=" + color +
                ", scheduleurl=" + scheduleurl +
                ", currentscheduledepisode=" + currentscheduledepisode +
                ", nextscheduledepisode=" + nextscheduledepisode +
                ", liveUrlPoorQuality=" + liveUrlPoorQuality +
                ", liveUrlLowQuality=" + liveUrlLowQuality +
                ", liveUrlNormalQuality=" + liveUrlNormalQuality +
                ", liveUrlHighQuality=" + liveUrlHighQuality +
                '}';
    }
}
