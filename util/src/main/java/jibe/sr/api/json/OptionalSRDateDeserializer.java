package jibe.sr.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Optional;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class OptionalSRDateDeserializer extends JsonDeserializer<Optional<DateTime>>
{
    private Pattern regex = Pattern.compile(".*\\((\\d+)\\).*");

    @Override
    public Optional<DateTime> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {
        String text = jsonParser.getText();
        Matcher matcher = regex.matcher(text);
        if (matcher.matches())
        {
            return Optional.of(new DateTime(Long.parseLong(matcher.group(1)), DateTimeZone.UTC));
        }
        return Optional.absent();
    }
}
