package jibe.sr.api.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Optional;
import org.joda.time.DateTime;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

/**
 *
 */
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SRSchedule
{
    private String title;

    private Optional<SRProgram> program = Optional.absent();
    private Optional<String> subtitle = Optional.absent();

    private Optional<String> description = Optional.absent();

    @JsonDeserialize(using = OptionalSRDateDeserializer.class)
    private Optional<DateTime> starttimeutc = Optional.absent();

    @JsonDeserialize(using = OptionalSRDateDeserializer.class)
    private Optional<DateTime> endtimeutc = Optional.absent();

    public Optional<String> getDescription()
    {
        return description;
    }

    public Optional<DateTime> getEndTime()
    {
        return endtimeutc;
    }

    public Optional<SRProgram> getProgram()
    {
        return program;
    }

    public Optional<DateTime> getStartTime()
    {
        return starttimeutc;
    }

    public Optional<String> getSubTitle()
    {
        return subtitle;
    }

    public String getTitle()
    {
        return title;
    }

    @Override
    public String toString()
    {
        return "SRSchedule{" +
                "title='" + title + '\'' +
                ", description=" + description +
                ", starttimeutc=" + starttimeutc +
                ", endtimeutc=" + endtimeutc +
                '}';
    }
}
