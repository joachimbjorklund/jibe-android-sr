package jibe.sr.api;

import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import jibe.sr.api.json.SRChannel;
import jibe.sr.api.json.SRChannelsSingle;
import jibe.sr.api.json.SRPlayList;
import jibe.sr.api.json.SRPlaylistEntity;
import jibe.sr.api.json.SRProgram;
import jibe.sr.api.json.SRProgramsSingle;
import jibe.sr.api.json.SRSchedule;
import jibe.sr.api.json.SRScheduledEpisodes;
import jibe.util.json.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static jibe.util.http.HttpUtil.executeRequestToResponseString;
import static jibe.util.http.HttpUtil.newHttpClient;

/**
 *
 */
public class SRApi
{
    public static final int POOR_QUALITY_KBPS = 32;
    public static final int LOW_QUALITY_KBPS = 64;
    public static final int NORMAL_QUALITY_KBPS = 128;
    public static final int HIGH_QUALITY_KBPS = 192;
    public static final String FORMAT_PARAMS = "format=json&indent=true&pagination=false";
    public static final String CHANNEL_ID_PARAM = "channelid=%d";
    public static final String HTTP_SR_API_V2 = "http://api.sr.se/api/v2";
    private static final String API_CHANNELS = "/channels";
    private static final String API_CHANNELS_CHANNEL_ID = API_CHANNELS + "/%d";
    private static final String API_PROGRAMS = "/programs";
    private static final String API_PROGRAMS_PROGRAM_ID = API_PROGRAMS + "/%d";
    private static final String API_PLAYLISTS_RIGHT_NOW = "/playlists/rightnow";
    private static final String API_PLAYLISTS_RIGHT_NOW_CHANNEL_ID = API_PLAYLISTS_RIGHT_NOW + "?" + CHANNEL_ID_PARAM;
    // http://api.sr.se/api/v2/programs/3718
    private static final String API_SCHEDULED_EPISODES = "/scheduledepisodes";
    private static final String API_SCHEDULED_EPISODES_CHANNEL_ID = API_SCHEDULED_EPISODES + "?" + CHANNEL_ID_PARAM;
    private static final String API_SCHEDULED_EPISODES_RIGHT_NOW = API_SCHEDULED_EPISODES + "/rightnow";
    private static final String API_SCHEDULED_EPISODES_RIGHT_NOW_CHANNEL_ID = API_SCHEDULED_EPISODES + "/rightnow" + "?" + CHANNEL_ID_PARAM;
    private static final String API_LIVE_M3U = "http://sverigesradio.se/topsy/direkt/%d-lo-mp3.m3u";
    private static Logger LOGGER = LoggerFactory.getLogger(SRApi.class.getSimpleName());
    private static Cache<Integer, SRChannel> channelsCache =
            CacheBuilder.newBuilder().expireAfterWrite(60 * 60, TimeUnit.SECONDS).build();

    private SRApi()
    {

    }

    private static String buildChannelsChannelIdURL(int channelId)
    {
        return format(HTTP_SR_API_V2 + API_CHANNELS_CHANNEL_ID + "?" + FORMAT_PARAMS, channelId);
    }

    private static String buildChannelsM3UURL(int channelId)
    {
        return format(API_LIVE_M3U, channelId);
    }

    private static String buildPlaylistsRightNowChannelIdURL(int channelId)
    {
        return format(HTTP_SR_API_V2 + API_PLAYLISTS_RIGHT_NOW_CHANNEL_ID + "&" + FORMAT_PARAMS, channelId);
    }

    private static String buildProgramsProgramIdURL(int programId)
    {
        return format(HTTP_SR_API_V2 + API_PROGRAMS_PROGRAM_ID + "?" + FORMAT_PARAMS, programId);
    }

    private static String buildScheduledEpisodesChannelIdURL(int channelId)
    {
        return format(HTTP_SR_API_V2 + API_SCHEDULED_EPISODES_CHANNEL_ID + "&" + FORMAT_PARAMS, channelId);
    }

    private static String buildScheduledEpisodesRightNowChannelIdURL(int channelId)
    {
        return format(HTTP_SR_API_V2 + API_SCHEDULED_EPISODES_RIGHT_NOW_CHANNEL_ID + "&" + FORMAT_PARAMS, channelId);
    }

    private static Optional<String> extractLinks(String data)
    {
        BufferedReader r = new BufferedReader(new StringReader(data));
        String s;
        try
        {
            while ((s = r.readLine()) != null)
            {
                if (s.startsWith("http://"))
                {
                    return Optional.of(s);
                }
            }
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage());
        }

        return Optional.absent();
    }

    private static Optional<SRChannel> fetchChannel(int channelId)
    {
        LOGGER.info("fetchChannel: " + channelId);

        String url = buildChannelsChannelIdURL(channelId);
        Optional<String> jsonData = executeRequestToResponseString(url);
        if (!jsonData.isPresent())
        {
            return Optional.absent();
        }


        SRChannel channel = JsonParser.<SRChannelsSingle>parse(jsonData.get(), SRChannelsSingle.class).getChannel();
        setLiveUrlsForChannel(channel);
        setRightNowEpisodesForChannel(channel);
        setRightNowPlaylistsForChannel(channel);

        LOGGER.info("fetched: " + channel.getId());
        return Optional.of(channel);
    }

    public static Optional<SRChannel> getChannel(final int channelId)
    {
        SRChannel srChannel;
        try
        {
            srChannel = channelsCache.get(channelId, new Callable<SRChannel>()
            {
                @Override
                public SRChannel call() throws Exception
                {
                    return fetchChannel(channelId).get();
                }
            });
        }
        catch (Exception e)
        {
            return Optional.absent();
        }

        return Optional.fromNullable(srChannel);
    }

    public static List<SRSchedule> getChannelsEpisodes(int channelId)
    {
        String url = buildScheduledEpisodesChannelIdURL(channelId);
        Optional<String> jsonData = executeRequestToResponseString(url);
        if (!jsonData.isPresent())
        {
            return Collections.emptyList();
        }

        return JsonParser.<SRScheduledEpisodes>parse(jsonData.get(), SRScheduledEpisodes.class).getEpisodes();
    }

    public static Optional<SRSchedule> getCurrentScheduledEpisode(int channelId)
    {
        String url = buildScheduledEpisodesRightNowChannelIdURL(channelId);
        Optional<String> jsonData = executeRequestToResponseString(url);
        if (!jsonData.isPresent())
        {
            return Optional.absent();
        }

        Optional<SRSchedule> foundEpisode = JsonParser.<SRChannelsSingle>parse(jsonData.get(), SRChannelsSingle.class).getChannel().getCurrentScheduledEpisode();
        if (!foundEpisode.isPresent())
        {
            return Optional.absent();
        }

        SRSchedule srSchedule = foundEpisode.get();
        if (srSchedule.getProgram().isPresent())
        {
            SRProgram srProgram = srSchedule.getProgram().get();
            if (!srProgram.getProgramImage().isPresent())
            {
                Optional<SRProgram> program = getProgram(srProgram.getId());
                if (program.isPresent())
                {
                    srProgram.setProgramImage(program.get().getProgramImage());
                }
            }
        }

        return foundEpisode;
    }

    public static Optional<SRSchedule> getNextScheduledEpisode(int channelId)
    {
        String url = buildScheduledEpisodesRightNowChannelIdURL(channelId);
        Optional<String> jsonData = executeRequestToResponseString(url);

        if (!jsonData.isPresent())
        {
            return Optional.absent();
        }

        return JsonParser.<SRChannelsSingle>parse(jsonData.get(), SRChannelsSingle.class).getChannel().getNextScheduledEpisode();
    }

    public static Optional<SRPlayList> getPlaylist(int channelId)
    {
        String url = buildPlaylistsRightNowChannelIdURL(channelId);
        Optional<String> jsonData = executeRequestToResponseString(url);

        if (!jsonData.isPresent())
        {
            return Optional.absent();
        }

        return JsonParser.<SRPlaylistEntity>parse(jsonData.get(), SRPlaylistEntity.class).getPlaylist();
    }

    public static Optional<SRProgram> getProgram(int programId)
    {
        String url = buildProgramsProgramIdURL(programId);
        Optional<String> jsonData = executeRequestToResponseString(url);
        if (!jsonData.isPresent())
        {
            return Optional.absent();
        }

        return JsonParser.<SRProgramsSingle>parse(jsonData.get(), SRProgramsSingle.class).getProgram();
    }

    public static Optional<SRChannel> refreshChannel(final int channelId)
    {
        LOGGER.info("refreshChannel: " + channelId);

        channelsCache.invalidate(channelId);
        return getChannel(channelId);
    }

    private static void setLiveUrlsForChannel(SRChannel channel)
    {
        String url = buildChannelsM3UURL(channel.getId());
        Optional<String> response = executeRequestToResponseString(url);
        if (!response.isPresent())
        {
            return;
        }


        Optional<String> foundLink = extractLinks(response.get());
        if (!foundLink.isPresent())
        {
            return;
        }

        String link = foundLink.get();
        link = link.substring(0, link.lastIndexOf('-') + 1);

        try
        {
            URL reference = new URL(link + POOR_QUALITY_KBPS);
            if (testUrl(reference))
            {
                channel.setLiveUrlPoorQuality(Optional.of(reference));
            }

            reference = new URL(link + LOW_QUALITY_KBPS);
            if (testUrl(reference))
            {
                channel.setLiveUrlLowQuality(Optional.of(reference));
            }

            reference = new URL(link + NORMAL_QUALITY_KBPS);
            if (testUrl(reference))
            {
                channel.setLiveUrlNormalQuality(Optional.of(reference));
            }

            reference = new URL(link + HIGH_QUALITY_KBPS);
            if (testUrl(reference))
            {
                channel.setLiveUrlHighQuality(Optional.of(reference));
            }
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage());
        }
    }

    private static void setRightNowEpisodesForChannel(SRChannel channel)
    {
        channel.setCurrentScheduledEpisode(getCurrentScheduledEpisode(channel.getId()));
        channel.setNextScheduledEpisode(getNextScheduledEpisode(channel.getId()));
    }

    private static void setRightNowPlaylistsForChannel(SRChannel channel)
    {
        channel.setPlayList(getPlaylist(channel.getId()));
    }

    private static boolean testUrl(URL url)
    {
        HttpURLConnection c = newHttpClient().open(url);
        try
        {
            return c.getResponseCode() == HttpURLConnection.HTTP_OK;
        }
        catch (IOException e)
        {
            return false;
        }
        finally
        {
            c.disconnect();
        }
    }

    private String buildChannelsURL()
    {
        return HTTP_SR_API_V2 + API_CHANNELS + "?" + FORMAT_PARAMS;
    }
}
