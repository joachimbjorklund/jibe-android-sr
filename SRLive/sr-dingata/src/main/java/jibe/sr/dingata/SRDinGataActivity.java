package jibe.sr.dingata;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class SRDinGataActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 2576;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(SRDinGataActivity.class, appName, R.drawable.sr_dingata_512_512, SR_API_CHANNEL));
    }
}
