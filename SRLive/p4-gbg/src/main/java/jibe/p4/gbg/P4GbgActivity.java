package jibe.p4.gbg;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class P4GbgActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 212;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(P4GbgActivity.class, appName, R.drawable.p4_gbg_512_512, SR_API_CHANNEL));
    }
}
