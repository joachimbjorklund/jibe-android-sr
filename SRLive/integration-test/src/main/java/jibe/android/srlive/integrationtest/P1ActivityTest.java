package jibe.android.srlive.integrationtest;

import android.net.Uri;
import android.widget.ImageView;
import com.google.common.base.Optional;
import jibe.android.service.HeartBeatService;
import jibe.android.streaming.MediaPlayerService;
import jibe.android.streaming.StreamingService;
import jibe.android.streaming.event.ActivityReadyEvent;
import jibe.android.streaming.event.ChannelColorEvent;
import jibe.android.streaming.event.ChannelImageEvent;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.event.HeartBeatEvent;
import jibe.android.streaming.event.ProgramImageEvent;
import jibe.android.streaming.sr.ChannelPrefsService;
import jibe.android.streaming.sr.SRInfoService;
import jibe.android.ui.ColorUtil;
import jibe.p1.P1Activity;
import jibe.sr.RightNowProgramInfoBuilder;

/**
 */
public class P1ActivityTest extends StreamingActivityTestCase<P1Activity>
{

    public P1ActivityTest()
    {
        super(P1Activity.class);
    }

    public void testFlow() throws Exception
    {
        // assert that all services are started
        assertActivityAndServicesReady();

        // assert that the channel is registered in SRInfoService
        assertChannelIsRegisteredAndTracked();

        // assert CenterFragment content
        assertCenterFragmentContent();

        // assert BottomFragment content
        assertBottomFragmentContent();

        // quit the app by double-click-back
        assertDoubleBackToExit();
    }

    private void assertActivityAndServicesReady() throws Exception
    {
        // check all services is started
        assertServicesExists();

        // assert that the activity sent its event to start the machinery going
        assertEvent(ActivityReadyEvent.class);

        // assert that the heartbeats are ticking
        assertEvent(HeartBeatEvent.class);
    }

    private void assertBottomFragmentContent() throws Exception
    {
        ProgramImageEvent programImageEvent = assertEvent(ProgramImageEvent.class);
        ChannelImageEvent channelImageEvent = assertEvent(ChannelImageEvent.class);

        if (programImageEvent.getImage().isPresent())
        {
            assertImageEquals(programImageEvent.getImage(), jibe.p1.R.id.bottomLayout_imageView);
        }
        else
        {
            assertImageEquals(channelImageEvent.getImage(), jibe.p1.R.id.bottomLayout_imageView);
        }
    }

    private void assertCenterFragmentContent() throws Exception
    {
        // assert ChannelImageEvent and ChannelColorEvent are sent from ChannelPrefsService
        ChannelInfoEvent channelInfoEvent = assertEvent(ChannelInfoEvent.class);
        ChannelImageEvent channelImageEvent = assertEvent(ChannelImageEvent.class);
        ChannelColorEvent channelColorEvent = assertEvent(ChannelColorEvent.class);
        ProgramImageEvent programImageEvent = assertEvent(ProgramImageEvent.class);

        // channel image assert that the images and color are there
        assertImageEquals(channelImageEvent.getImage(), jibe.p1.R.id.centerLayout_channelImageView);
        Optional<Integer> channelColor = channelColorEvent.getChannelColor();
        if (channelColor.isPresent())
        {
            assertImageBgColorEquals(ColorUtil.darker(channelColor.get()), jibe.p1.R.id.centerLayout_channelImageView);
        }

        // program image assert that the images and color are there
        assertImageEquals(programImageEvent.getImage(), jibe.p1.R.id.centerLayout_programImageView);
        if (channelColor.isPresent())
        {
            assertImageBgColorEquals(ColorUtil.darker(channelColor.get()), jibe.p1.R.id.centerLayout_programImageView);
        }

        String text = RightNowProgramInfoBuilder.buildRightNowInfoTextCurrent(channelInfoEvent.getChannel(),
                RightNowProgramInfoBuilder.Format.COMPACT).toString();
        assertTrue(getSolo().waitForText(text, 1, 1000));
    }

    private void assertChannelIsRegisteredAndTracked() throws Exception
    {
        assertTrue(getService(SRInfoService.class).isChannelRegistred(P1Activity.SR_API_CHANNEL));
        assertEvent(ChannelInfoEvent.class);
    }

    private void assertDoubleBackToExit() throws Exception
    {
        assertEquals(0, getActivity().getBackPressTwiceToExitHandler().getBackPressed());

        getSolo().goBack();
        assertTrue(getSolo().waitForText(getSolo().getString(jibe.p1.R.string.oneMoreTimeToExit), 1, 1000));
        assertEquals(1, getActivity().getBackPressTwiceToExitHandler().getBackPressed());

        getSolo().goBack();
        assertEquals(2, getActivity().getBackPressTwiceToExitHandler().getBackPressed());
    }

    private void assertImageBgColorEquals(int bgColor, int imageViewId)
    {
        ImageView view = (ImageView) getSolo().getView(imageViewId);
        assertEquals(bgColor, view.getTag(jibe.android.R.id.imageBGColorTagKey));
    }

    private void assertImageEquals(Optional<Uri> imageUri, int imageViewId)
    {
        if (!imageUri.isPresent())
        {
            return;
        }

        ImageView view = (ImageView) getSolo().getView(imageViewId);
        assertEquals(imageUri.get(), view.getTag(jibe.android.R.id.imageUriTagKey));
    }

    private void assertServicesExists() throws Exception
    {
        assertTrue(findService(MediaPlayerService.class).isPresent());
        assertTrue(findService(StreamingService.class).isPresent());
        assertTrue(findService(SRInfoService.class).isPresent());
        assertTrue(findService(ChannelPrefsService.class).isPresent());
        assertTrue(findService(HeartBeatService.class).isPresent());
    }
}
