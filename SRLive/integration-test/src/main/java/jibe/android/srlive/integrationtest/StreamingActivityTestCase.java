package jibe.android.srlive.integrationtest;

import android.test.ActivityInstrumentationTestCase2;
import com.google.common.base.Optional;
import com.jayway.awaitility.Awaitility;
import com.jayway.awaitility.Duration;
import com.robotium.solo.Solo;
import jibe.android.service.HeartBeatService;
import jibe.android.service.JibeService;
import jibe.android.service.LocalJibeServiceConnection;
import jibe.android.streaming.MediaPlayerService;
import jibe.android.streaming.StreamingService;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.sr.ChannelPrefsService;
import jibe.android.streaming.sr.SRInfoService;
import jibe.p1.P1Activity;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

/**
 *
 */
public abstract class StreamingActivityTestCase<T extends StreamingActivity> extends ActivityInstrumentationTestCase2<T>
{
    private Solo solo;
    private Logger logger;
    private ApplicationEventBus eventBus;
    private Map<Class<? extends JibeService>, JibeService> servicesMap;

    public StreamingActivityTestCase(Class<T> activityClass)
    {
        super(activityClass);
    }

    @Override
    public void tearDown() throws Exception
    {
        solo.finishOpenedActivities();
    }

    private Map<Class<? extends JibeService>, JibeService> assertServiceMap(final Class<? extends JibeService>... cls) throws Exception
    {
        logger.info("assertServiceMap");
        Map<Class<? extends JibeService>, JibeService> answer = new HashMap<>();

        for (LocalJibeServiceConnection s : getActivity().getServiceConnections())
        {
            answer.put(s.getServiceClass(), s.getService());
        }

        for (Class<? extends JibeService> c : cls)
        {
            if (!answer.containsKey(c))
            {
                fail("answer: " + answer + " does not contain class: " + c);
            }
        }

        return answer;
    }

    public interface EventSubscriber
    {
        @SuppressWarnings("unused")
        public void onEvent(Object event);
    }

    @Override
    protected void setUp() throws Exception
    {
        solo = new Solo(getInstrumentation(), getActivity());
        assertTrue(solo.waitForActivity(P1Activity.class, 20000));
        logger = getActivity().getLogger(P1ActivityTest.class);
        eventBus = getActivity().getEventBus();

        servicesMap = assertServiceMap(
                StreamingService.class,
                MediaPlayerService.class,
                SRInfoService.class,
                ChannelPrefsService.class,
                HeartBeatService.class);
    }

    protected <T extends JibeService> T getService(Class<T> serviceClass)
    {
        return findService(serviceClass).get();
    }

    protected <T extends JibeService> Optional<T> findService(Class<T> serviceClass)
    {
        for (Class<? extends JibeService> s : servicesMap.keySet())
        {
            if (s.isAssignableFrom(serviceClass))
            {
                return Optional.of((T) servicesMap.get(s));
            }
        }
        return Optional.absent();
    }

    protected Solo getSolo()
    {
        return solo;
    }

    protected Logger getLogger()
    {
        return logger;
    }

    protected ApplicationEventBus getEventBus()
    {
        return eventBus;
    }

    protected <T> T assertEvent(final Class<T> eventClass) throws Exception
    {
        Optional<T> stickyEvent = eventBus.getStickyEvent(eventClass);
        if (stickyEvent.isPresent())
        {
            return stickyEvent.get();
        }

        final AtomicReference<Object> answer = new AtomicReference<>(null);
        EventSubscriber subscriber = new EventSubscriber()
        {
            @Override
            public void onEvent(Object event)
            {
                logger.info("onEvent: " + event);
                if (event.getClass().isAssignableFrom(eventClass))
                {
                    answer.set(event);
                }
            }
        };

        eventBus.registerSticky(subscriber);

        try
        {
            Awaitility.await().atMost(Duration.TEN_SECONDS).until(new Callable<Boolean>()
            {
                @Override
                public Boolean call()
                {
                    logger.info("call");
                    return answer.get() != null;
                }
            });
        }
        finally
        {
            eventBus.unregister(subscriber);
        }

        return (T) answer.get();
    }
}
