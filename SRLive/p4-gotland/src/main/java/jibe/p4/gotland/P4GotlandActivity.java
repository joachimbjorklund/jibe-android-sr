package jibe.p4.gotland;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class P4GotlandActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 205;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(P4GotlandActivity.class, appName, R.drawable.p4_gotland_512_512, SR_API_CHANNEL));
    }
}
