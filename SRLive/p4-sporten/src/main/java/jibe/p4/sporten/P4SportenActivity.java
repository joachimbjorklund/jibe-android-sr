package jibe.p4.sporten;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class P4SportenActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 179;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(P4SportenActivity.class, appName, R.drawable.p4_sporten_512_512, SR_API_CHANNEL));
    }
}
