package jibe.android.service;

import jibe.android.streaming.event.ActivityReadyEvent;
import jibe.android.streaming.event.HeartBeatEvent;
import org.joda.time.Seconds;

/**
 *
 */
public class HeartBeatService extends JibeService
{
    @SuppressWarnings("unused")
    public void onEvent(ActivityReadyEvent event)
    {
        executorService.scheduleWithFixedDelay(new Runnable()
        {
            @Override
            public void run()
            {
                eventBus.post(new HeartBeatEvent());
            }
        }, Seconds.seconds(2));
    }
}
