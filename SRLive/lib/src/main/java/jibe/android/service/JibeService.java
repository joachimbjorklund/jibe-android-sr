package jibe.android.service;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import com.google.inject.Inject;
import jibe.android.streaming.application.ApplicationEventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import roboguice.service.RoboService;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class JibeService extends RoboService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JibeService.class.getSimpleName());

    private final List<LocalJibeServiceConnection> serviceConnections = new ArrayList<>();

    @Inject
    protected ApplicationEventBus eventBus;

    @Inject
    protected JibeExecutorService executorService;

    @Inject
    private ConnectivityManager connectivityManager;

    public boolean haveConnectivity()
    {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null) && networkInfo.isConnectedOrConnecting();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return new LocalJibeServiceBinder<>(this);
    }

    @Override
    public void onCreate()
    {
        LOGGER.info("onCreate");
        super.onCreate();

        eventBus.registerSticky(this);

        createServices();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        destroyServices();
    }

    @SuppressWarnings("unused")
    public void onEvent(ApplicationEventBus.NoOpEvent event)
    {
    }

    private void destroyServices()
    {
        for (LocalJibeServiceConnection c : serviceConnections)
        {
            try
            {
                unbindService(c);
            }
            catch (Exception e)
            {
                LOGGER.error("error on unbinding service: " + c.getServiceClass());
            }
        }
    }

    protected void createServices()
    {
    }

    protected void bindJibeService(Class<? extends JibeService> service)
    {
        LocalJibeServiceConnection c = new LocalJibeServiceConnection(service);
        if (bindService(new Intent(this, service), c, Context.BIND_AUTO_CREATE))
        {
            serviceConnections.add(c);
        }
    }
}
