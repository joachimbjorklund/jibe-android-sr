package jibe.android.service;

import android.os.Binder;

/**
 *
 */
public class LocalJibeServiceBinder<T extends JibeService> extends Binder
{
    private final T service;

    public LocalJibeServiceBinder(T service)
    {
        this.service = service;
    }

    public T getService()
    {

        return service;
    }
}
