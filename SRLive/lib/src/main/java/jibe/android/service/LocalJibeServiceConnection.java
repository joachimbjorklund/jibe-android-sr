package jibe.android.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
@Singleton
public class LocalJibeServiceConnection<T extends JibeService> implements ServiceConnection
{
    private static final Logger LOGGER = LoggerFactory.getLogger(LocalJibeServiceConnection.class.getSimpleName());

    private final Class<? extends JibeService> serviceClass;
    private T service;

    public LocalJibeServiceConnection(Class<? extends JibeService> serviceClass)
    {
        this.serviceClass = serviceClass;
    }

    public T getService()
    {
        return service;
    }

    public Class<? extends JibeService> getServiceClass()
    {
        return serviceClass;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder binder)
    {
        LOGGER.info("onServiceConnected: " + name + ", " + binder + ", (" + serviceClass + ")");
        service = (T) ((LocalJibeServiceBinder) binder).getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        LOGGER.info("onServiceDisconnected: " + name + " (" + serviceClass + ")");
    }
}
