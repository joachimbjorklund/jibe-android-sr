package jibe.android.service;

import com.google.inject.Singleton;
import org.joda.time.Seconds;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 */
@Singleton
public class JibeExecutorService
{
    private final ScheduledExecutorService executorService =
            Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());

    public void schedule(Runnable runnable, Seconds delay)
    {
        executorService.schedule(runnable, delay.getSeconds(), TimeUnit.SECONDS);
    }

    public <T> ScheduledFuture<T> schedule(Callable<T> callable)
    {
        return schedule(callable, Seconds.ZERO);
    }

    public <T> ScheduledFuture<T> schedule(Callable<T> callable, Seconds delay)
    {
        return executorService.schedule(callable, delay.getSeconds(), TimeUnit.SECONDS);
    }

    public void scheduleWithFixedDelay(Runnable runnable, Seconds delay)
    {
        executorService.scheduleWithFixedDelay(runnable, 0, delay.getSeconds(), TimeUnit.SECONDS);
    }

    public void shutdown()
    {
        executorService.shutdownNow();
    }
}
