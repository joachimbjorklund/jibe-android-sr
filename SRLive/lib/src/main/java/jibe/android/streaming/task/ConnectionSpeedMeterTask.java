package jibe.android.streaming.task;

import com.google.common.base.Optional;
import jibe.android.streaming.StreamingService;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.event.NetworkInfoEvent;
import jibe.util.http.HttpUtil;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

/**
 *
 */
public class ConnectionSpeedMeterTask implements Runnable
{
    public static final int SAMPLE_SIZE = 5;
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionSpeedMeterTask.class.getSimpleName());

    private StreamingService streamingService;
    private URL testEndpoint;

    private ApplicationEventBus eventBus;

    private LinkedList<BigDecimal> samples = new LinkedList<>();

    public ConnectionSpeedMeterTask(StreamingService streamingService, ApplicationEventBus eventBus, String testEndpoint)
    {
        this.streamingService = streamingService;
        this.eventBus = eventBus;
        this.eventBus.registerSticky(this);
        try
        {
            this.testEndpoint = new URL(testEndpoint);
        }
        catch (MalformedURLException e)
        {
        }
    }

    @SuppressWarnings("unused")
    public void onEventAsync(NetworkInfoEvent event)
    {
        if (!streamingService.haveConnectivity())
        {
            reset();
        }
    }

    public void reset()
    {
        samples.clear();
    }

    @Override
    public void run()
    {
        if (!streamingService.haveConnectivity())
        {
            LOGGER.info("no connectivity");
            eventBus.postSticky(new Kbps(BigDecimal.ZERO));
            reset();
            return;
        }

        DateTime start;
        int nbrRead;
        InputStream is = null;
        try
        {
            Optional<HttpURLConnection> connection = HttpUtil.openConnection(testEndpoint.toExternalForm(), 200);
            if (!connection.isPresent())
            {
                LOGGER.info("no connection");
                eventBus.postSticky(new Kbps(BigDecimal.ZERO));
                reset();
                return;
            }
            start = new DateTime();
            boolean stop = false;
            int read = 0;
            Duration duration = new Duration(0);
            is = connection.get().getInputStream();
            while (!stop && (nbrRead = is.read(new byte[1024])) != -1)
            {
                read += nbrRead;
                duration = new Duration(start, new DateTime());
                if (duration.getStandardSeconds() >= 2)
                {
                    stop = true;
                }
            }

            double kb = (double) read / 1024d;
            double elapsedSeconds = (double) duration.getMillis() / 1000d;
            double kbps = kb / elapsedSeconds;

            BigDecimal Kbps = new BigDecimal(kbps).setScale(2, RoundingMode.HALF_DOWN);

            if (samples.size() == SAMPLE_SIZE)
            {
                samples.removeLast();
            }
            samples.addFirst(Kbps);

            eventBus.postSticky(new Kbps(average()));
        }
        catch (Exception e)
        {
            LOGGER.error(e.getMessage());
        }
        finally
        {
            try
            {
                if (is != null)
                {
                    is.close();
                }
            }
            catch (IOException e)
            {
            }
        }
    }

    private BigDecimal average()
    {
        int size = samples.size();
        BigDecimal answer = BigDecimal.ZERO.setScale(2);
        for (BigDecimal d : samples)
        {
            answer = answer.add(d);
        }

        return answer.divide(new BigDecimal(size), RoundingMode.HALF_DOWN);
    }

    public class Kbps
    {
        private final BigDecimal kbps;

        public Kbps(BigDecimal kbps)
        {
            this.kbps = kbps;
        }

        public BigDecimal getValue()
        {
            return kbps;
        }

        @Override
        public String toString()
        {
            return "Kbps{" +
                    "kbps=" + kbps +
                    '}';
        }
    }
}
