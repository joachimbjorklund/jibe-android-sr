package jibe.android.streaming.fsm;

/**
 *
 */
public enum StreamingServiceTrigger
{
    PREPARE_TRIGGER,
    STOP_PLAYING, ERROR, PREPARED
}
