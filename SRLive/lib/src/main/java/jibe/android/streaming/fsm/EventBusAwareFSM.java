package jibe.android.streaming.fsm;

import android.content.Context;
import com.github.oxo42.stateless4j.StateConfiguration;
import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.delegates.Action;
import com.google.inject.Inject;
import jibe.android.streaming.application.ApplicationEventBus;
import roboguice.RoboGuice;

/**
 *
 */
public abstract class EventBusAwareFSM<S extends Enum, T extends Enum> extends StateMachine<S, T>
{
    @Inject
    protected ApplicationEventBus eventBus;

    public EventBusAwareFSM(S initialState, Context context)
    {
        super(initialState);

        RoboGuice.getInjector(context).injectMembersWithoutViews(this);
        eventBus.registerSticky(this);
    }

    public StateConfiguration configure(final S state)
    {
        StateConfiguration stateConfiguration = super.configure(state);
        stateConfiguration.onEntry(new Action()
        {
            @Override
            public void doIt()
            {
                eventBus.post(new OnStateEntryEvent(state));
            }
        });
        stateConfiguration.onExit(new Action()
        {
            @Override
            public void doIt()
            {
                eventBus.post(new OnStateExitEvent(state));
            }
        });

        return stateConfiguration;
    }

    public class OnStateEvent
    {
        private final S state;

        public OnStateEvent(S state)
        {
            this.state = state;
        }

        public S getState()
        {
            return state;
        }
    }

    public class OnStateEntryEvent extends OnStateEvent
    {
        public OnStateEntryEvent(S state)
        {
            super(state);
        }
    }

    public class OnStateExitEvent extends OnStateEvent
    {
        public OnStateExitEvent(S state)
        {
            super(state);
        }
    }
}
