package jibe.android.streaming.monitor;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.event.AudioBecomingNoisyEvent;
import jibe.android.streaming.event.NetworkInfoEvent;
import roboguice.receiver.RoboBroadcastReceiver;

import static android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY;
import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

/**
 *
 */
public class JibeBroadcastReceiver extends RoboBroadcastReceiver
{
    @Inject
    private static ConnectivityManager connectivityManager;

    @Inject
    private ApplicationEventBus eventBus;

    private Optional<NetworkInfo> getNetworkInfo()
    {
        if (connectivityManager == null)
        {
            return Optional.absent();
        }
        return Optional.fromNullable(connectivityManager.getActiveNetworkInfo());
    }

    @Override
    protected void handleReceive(Context context, Intent intent)
    {
        if (intent.getAction().equals(CONNECTIVITY_ACTION) || intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION))
        {
            eventBus.postSticky(new NetworkInfoEvent(getNetworkInfo()));
        }
        else if (intent.getAction().equals(ACTION_AUDIO_BECOMING_NOISY))
        {
            eventBus.post(new AudioBecomingNoisyEvent());
        }
    }
}
