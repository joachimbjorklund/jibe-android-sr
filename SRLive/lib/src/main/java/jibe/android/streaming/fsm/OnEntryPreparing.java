package jibe.android.streaming.fsm;

import android.media.AudioManager;
import android.media.MediaPlayer;
import com.github.oxo42.stateless4j.delegates.Action;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import jibe.android.streaming.MediaPlayerPreparedListener;
import jibe.android.streaming.MediaQualityController;
import jibe.android.streaming.application.ApplicationEventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 */
class OnEntryPreparing implements Action
{
    private final static Logger LOGGER = LoggerFactory.getLogger(OnEntryPreparing.class.getSimpleName());
    private final StreamingServiceFSM streamingServiceFSM;
    @Inject
    private ApplicationEventBus eventBus;
    @Inject
    private MediaPlayerPreparedListener preparedListener;
    @Inject
    private MediaQualityController mediaQualityController;

    @Inject
    public OnEntryPreparing(StreamingServiceFSM streamingServiceFSM)
    {
        this.streamingServiceFSM = streamingServiceFSM;
    }

    @Override
    public void doIt()
    {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(preparedListener);
//                mediaPlayer.setOnCompletionListener(this);
//                mediaPlayer.setOnInfoListener(this);
//                mediaPlayer.setOnErrorListener(this);
//                mediaPlayer.setOnBufferingUpdateListener(this);

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setLooping(false);
//        mediaPlayer.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK);

        Optional<MediaQualityController.PreferredMedia> preferredMediaQuality = mediaQualityController.getPreferredMediaQuality();
        try
        {
            mediaPlayer.setDataSource(preferredMediaQuality.get().getUrl().toExternalForm()); // this should be guarded
        }
        catch (IOException e)
        {
            eventBus.post(new ExceptionEvent(e));
        }

        mediaPlayer.prepareAsync();
    }

    public class ExceptionEvent
    {

        private final Exception exception;

        public ExceptionEvent(Exception exception)
        {
            this.exception = exception;
        }

        public Exception getException()
        {
            return exception;
        }
    }
}
