package jibe.android.streaming;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import jibe.android.R;
import jibe.android.service.JibeService;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import jibe.android.streaming.event.AudioBecomingNoisyEvent;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.event.HeartBeatEvent;
import jibe.android.streaming.event.MediaPlayerPaused;
import jibe.android.streaming.event.MediaPlayerStarted;
import jibe.android.streaming.fsm.AudioFocusFSM;
import jibe.android.streaming.fsm.StreamingServiceFSM;
import jibe.android.streaming.sr.ChannelPrefsService;
import jibe.android.streaming.task.ConnectionSpeedMeterTask;
import jibe.sr.RightNowProgramInfoBuilder;
import jibe.sr.api.json.SRChannel;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.net.URL;

import static jibe.android.streaming.fsm.StreamingServiceTrigger.PREPARE_TRIGGER;
import static jibe.sr.RightNowProgramInfoBuilder.buildRightNowInfoTextCurrent;

/**
 *
 */
public class StreamingService extends JibeService
{
    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";
    private static final Logger LOGGER = LoggerFactory.getLogger(StreamingService.class.getSimpleName());
    public Optional<Integer> notificationId = Optional.absent();

    private Optional<MediaPlayer> mediaPlayer = Optional.absent();
    private Optional<ChannelInfoEvent> channelInfo = Optional.absent();
    private Optional<BigDecimal> kbps = Optional.absent();
    private Optional<URL> mediaUrl = Optional.absent();
    private Optional<Notification> notification = Optional.absent();

    @Inject
    private AudioFocusController audioFocusController;

    @Inject
    private MediaQualityController mediaQualityController;

    @Inject
    private StreamingServiceFSM streamingServiceFSM;

    @Override
    public void onCreate()
    {
        super.onCreate();

//        streamingServiceFSM.configure();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        releasePlayer();
    }

//    @SuppressWarnings("unused")
//    public void onEvent(ConnectionSpeedMeterTask.Kbps kbps)
//    {
//        this.kbps = Optional.of(kbps.getValue());
//        if (isPlaying() && mediaUrl.isPresent() && haveChannelInfo())
//        {
//            Optional<MediaQualityController.PreferredMedia> prepare =
//                    mediaQualityController.getPreferredMediaQuality(
//                            channelInfo.get().getChannel().getLiveUrlHighQuality(),
//                            channelInfo.get().getChannel().getLiveUrlNormalQuality(),
//                            channelInfo.get().getChannel().getLiveUrlLowQuality(),
//                            channelInfo.get().getChannel().getLiveUrlPoorQuality());
//
//            if (prepare.isPresent() && !mediaUrl.get().equals(prepare.get().getUrl()))
//            {
//                eventBus.post(new MediaPlayerService.Prepare(prepare.get().getDescription(), prepare.get().getUrl()));
//            }
//            else if (isPlaying() && !prepare.isPresent())
//            {
//                releasePlayer();
//            }
//        }
//    }

    @SuppressWarnings("unused")
    public void onEvent(MediaPlayerService.MediaPlayerReleasedEvent mediaPlayerReleasedEvent)
    {
        LOGGER.info("mediaPlayerReleasedEvent: " + mediaPlayerReleasedEvent);
        if (mediaPlayer.isPresent() && mediaPlayer.get().equals(mediaPlayerReleasedEvent.getMediaPlayer()))
        {
            LOGGER.info("my player was released:");
            mediaPlayer = Optional.absent();
            mediaUrl = Optional.absent();
        }
        else
        {
            LOGGER.info("some other player was released");
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(ChannelInfoEvent channelInfo)
    {
        this.channelInfo = Optional.of(channelInfo);
    }

    @SuppressWarnings("unused")
    public void onEvent(ActivityReadyEvent event)
    {
        sendNotification(event.getActivityClass(), event.getTitle(), event.getIconResourceId());
        audioFocusController.requestAudioFocus();
    }

    @SuppressWarnings("unused")
    public void onEvent(AudioFocusFSM.State event)
    {

    }

    @SuppressWarnings("unused")
    public void onEvent(HeartBeatEvent event)
    {
        streamingServiceFSM.fire(PREPARE_TRIGGER); // eager to prepare new media

        if (!notification.isPresent())
        {
            Optional<ActivityReadyEvent> activityInfo = eventBus.getStickyEvent(ActivityReadyEvent.class);
            if (activityInfo.isPresent())
            {
                sendNotification(activityInfo.get().getActivityClass(), activityInfo.get().getTitle(), activityInfo.get().getIconResourceId());
            }
        }
    }

    @SuppressWarnings("unused")
    public void onEventAsync(MediaPlayerStartRequest start)
    {
        startPlayer();
    }

    @SuppressWarnings("unused")
    public void onEventAsync(MediaPlayerPauseRequest pause)
    {
        pausePlayer();
    }

    @SuppressWarnings("unused")
    public void onEventAsync(AudioBecomingNoisyEvent event)
    {
        pausePlayer();
    }

    private String createNotificationTickerText()
    {
        if (!haveChannelInfo())
        {
            return EMPTY_STRING;
        }

        SRChannel channel = channelInfo.get().getChannel();
        StringBuilder sb = new StringBuilder()
                .append(getString(R.string.program_info_on_air))
                .append(SPACE_STRING)
                .append(buildRightNowInfoTextCurrent(channel, RightNowProgramInfoBuilder.Format.COMPACT))
//                .append(SPACE_STRING)
//                .append(getString(R.string.program_info_next))
//                .append(buildRightNowInfoTextNext(channel, RightNowProgramInfoBuilder.Format.COMPACT))
                ;

        return sb.toString();
    }

    private boolean haveAudioFocus()
    {
        return audioFocusController.haveAudioFocus();
    }

    private boolean haveChannelInfo()
    {
        return channelInfo.isPresent() || (channelInfo = eventBus.getStickyEvent(ChannelInfoEvent.class)).isPresent();
    }

    private void pausePlayer()
    {
        if (!mediaPlayer.isPresent())
        {
            return;
        }

        if (!mediaPlayer.get().isPlaying())
        {
            return;
        }

        mediaPlayer.get().pause();
        eventBus.postSticky(new MediaPlayerPaused());
    }

    private void releasePlayer()
    {
        if (mediaPlayer.isPresent())
        {
            mediaPlayer.get().release();
            mediaPlayer = Optional.absent();
        }
    }

    private void sendNotification(Class<? extends StreamingActivity> activityClass, String title, int iconResourceId)
    {
        Notification notification = new Notification();
        this.notification = Optional.of(notification);

        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), activityClass), PendingIntent.FLAG_UPDATE_CURRENT);
        String notificationTickerText = createNotificationTickerText();
        notification.tickerText = title;
        notification.icon = iconResourceId;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.setLatestEventInfo(getApplicationContext(), title, notificationTickerText, pi);

        if (!notificationId.isPresent())
        {
            notificationId = Optional.of(1);

            startForeground(notificationId.get(), notification);

            if (Strings.isNullOrEmpty(notificationTickerText))
            {
                this.notification = Optional.absent();
            }
        }
        else
        {
            if (!Strings.isNullOrEmpty(notificationTickerText))
            {
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(notificationId.get(), notification);
            }
            else
            {
                this.notification = Optional.absent();
            }
        }
    }

    private void startPlayer()
    {
        if (!audioFocusController.requestAudioFocus())
        {
            return;
        }

        if (!mediaPlayer.isPresent())
        {
            return;
        }

        mediaPlayer.get().setVolume(1.0f, 1.0f);
        if (mediaPlayer.get().isPlaying())
        {
            return;
        }

        mediaPlayer.get().start();
        eventBus.post(new MediaPlayerStarted());
    }

    protected void createServices()
    {
        super.createServices();

        bindJibeService(ChannelPrefsService.class);
        bindJibeService(MediaPlayerService.class);

        executorService.scheduleWithFixedDelay(new ConnectionSpeedMeterTask(this, eventBus, "http://sverigesradio.se"), Seconds.seconds(10));
    }
}
