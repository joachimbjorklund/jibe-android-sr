package jibe.android.streaming.application;

import com.google.common.base.Optional;
import com.google.inject.Singleton;
import de.greenrobot.event.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
@Singleton
public class ApplicationEventBus
{
    private static Logger LOGGER = LoggerFactory.getLogger(ApplicationEventBus.class.getSimpleName());

    private static EventBus eventBus;

    static
    {
        if (eventBus == null)
        {
//            EventBusExecutorServiceConfigurer.configureEventBus();
            eventBus = EventBus.getDefault();
        }
    }

    public <T extends Object> Optional<T> getStickyEvent(Class<T> eventType)
    {
        return Optional.fromNullable(eventBus.getStickyEvent(eventType));
    }

    public boolean isRegistered(Object subscriber)
    {
        return eventBus.isRegistered(subscriber);
    }

    public void post(Object event)
    {
        eventBus.post(event);
    }

    public void postSticky(Object event)
    {
        eventBus.postSticky(event);
    }

    public boolean register(Object subscriber)
    {
        if (eventBus.isRegistered(subscriber))
        {
            return false;
        }

        eventBus.register(subscriber);
        return true;
    }

    public boolean registerSticky(Object subscriber)
    {
        if (eventBus.isRegistered(subscriber))
        {
            return false;
        }

        eventBus.registerSticky(subscriber);
        return true;
    }

    public void removeAllStickyEvents()
    {
        eventBus.removeAllStickyEvents();
    }

    public void removeStickyEvent(Object event)
    {
        eventBus.removeStickyEvent(event);
    }

    public void unregister(Object subscriber)
    {
        eventBus.unregister(subscriber);
    }

    public class NoOpEvent
    {
    }
}
