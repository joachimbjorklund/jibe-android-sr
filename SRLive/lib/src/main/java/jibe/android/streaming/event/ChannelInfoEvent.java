package jibe.android.streaming.event;

import jibe.sr.api.json.SRChannel;

/**
 *
 */
public class ChannelInfoEvent
{
    private final SRChannel channel;

    public ChannelInfoEvent(SRChannel channel)
    {
        this.channel = channel;
    }

    public SRChannel getChannel()
    {
        return channel;
    }

    @Override
    public String toString()
    {
        return "ChannelInfo{" +
                "channel=" + channel +
                '}';
    }
}
