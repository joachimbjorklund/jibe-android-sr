package jibe.android.streaming.event;

import com.google.common.base.Optional;

/**
 *
 */
public class ChannelColorEvent
{
    private final Optional<Integer> channelColor;

    public ChannelColorEvent(Optional<Integer> channelColor)
    {
        this.channelColor = channelColor;
    }

    public Optional<Integer> getChannelColor()
    {
        return channelColor;
    }
}
