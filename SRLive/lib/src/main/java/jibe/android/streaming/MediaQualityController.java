package jibe.android.streaming;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.monitor.ConnectivityController;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@Singleton
public class MediaQualityController
{
    private static final int POOR_MEDIA_QUALITY_THRESHOLD = 32;
    private static final int LOW_MEDIA_QUALITY_THRESHOLD = 64;
    private static final int NORMAL_MEDIA_QUALITY_THRESHOLD = 128;
    private static final int HIGH_MEDIA_QUALITY_THRESHOLD = 192;

    @Inject
    private ConnectivityController connectivityController;

    @Inject
    private ApplicationEventBus eventBus;

    public Optional<PreferredMedia> getPreferredMediaQuality()
    {
        Optional<ChannelInfoEvent> gotEvent = eventBus.getStickyEvent(ChannelInfoEvent.class);
        if (!gotEvent.isPresent())
        {
            return Optional.absent();
        }

        ChannelInfoEvent channelInfoEvent = gotEvent.get();

        return getPreferredMediaQuality(channelInfoEvent.getChannel().getLiveUrlHighQuality(),
                channelInfoEvent.getChannel().getLiveUrlNormalQuality(),
                channelInfoEvent.getChannel().getLiveUrlLowQuality(),
                channelInfoEvent.getChannel().getLiveUrlPoorQuality());
    }

    public Optional<PreferredMedia> getPreferredMediaQuality(Optional<URL> urlHighQuality, Optional<URL> urlNormalQuality, Optional<URL> urlLowQuality, Optional<URL> urlPoorQuality)
    {
        if (!connectivityController.getConnectivitySpeed().isPresent())
        {
            return Optional.absent();
        }

        Map<Optional<URL>, Integer> qualityMap = new HashMap<>();
        qualityMap.put(urlHighQuality, HIGH_MEDIA_QUALITY_THRESHOLD);
        qualityMap.put(urlNormalQuality, NORMAL_MEDIA_QUALITY_THRESHOLD);
        qualityMap.put(urlLowQuality, LOW_MEDIA_QUALITY_THRESHOLD);
        qualityMap.put(urlPoorQuality, POOR_MEDIA_QUALITY_THRESHOLD);

        Integer bpsValue = connectivityController.getConnectivitySpeed().get().intValue();
        Optional<URL> mediaUrl;
        int preferredMediaQuality = getPreferredMediaQualityFromUser();
        if ((preferredMediaQuality >= HIGH_MEDIA_QUALITY_THRESHOLD) && (bpsValue >= HIGH_MEDIA_QUALITY_THRESHOLD))
        {
            mediaUrl = pickFirstPresent(urlHighQuality, urlNormalQuality, urlLowQuality, urlPoorQuality);
        }
        else if ((preferredMediaQuality >= NORMAL_MEDIA_QUALITY_THRESHOLD) && (bpsValue >= NORMAL_MEDIA_QUALITY_THRESHOLD))
        {
            mediaUrl = pickFirstPresent(urlNormalQuality, urlLowQuality, urlPoorQuality);
        }
        else if ((preferredMediaQuality >= LOW_MEDIA_QUALITY_THRESHOLD) && (bpsValue >= LOW_MEDIA_QUALITY_THRESHOLD))
        {
            mediaUrl = pickFirstPresent(urlLowQuality, urlPoorQuality);
        }
        else
        {
            mediaUrl = pickFirstPresent(urlPoorQuality);
        }

        if (mediaUrl.isPresent())
        {
            return Optional.of(new PreferredMedia(qualityMap.get(mediaUrl).toString(), mediaUrl.get()));
        }
        return Optional.absent();
    }

    private int getPreferredMediaQualityFromUser() // TODO: should be settable as preference
    {
        if (connectivityController.isOnWifi())
        {
            return HIGH_MEDIA_QUALITY_THRESHOLD;
        }
        else
        {
            return LOW_MEDIA_QUALITY_THRESHOLD;
        }
    }

    private Optional<URL> pickFirstPresent(Optional<URL>... urls)
    {
        for (Optional<URL> url : urls)
        {
            if (url.isPresent())
            {
                return url;
            }
        }

        return Optional.absent();
    }

    public class PreferredMedia
    {
        private final URL url;
        private final String description;

        public PreferredMedia(String description, URL url)
        {
            this.url = url;
            this.description = description;
        }

        public String getDescription()
        {
            return description;
        }

        public URL getUrl()
        {
            return url;
        }
    }
}
