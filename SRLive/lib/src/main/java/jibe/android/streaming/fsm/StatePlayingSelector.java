package jibe.android.streaming.fsm;

import android.media.MediaPlayer;
import com.github.oxo42.stateless4j.delegates.Func2;
import com.google.inject.Inject;

import javax.annotation.Nonnull;

import static jibe.android.streaming.fsm.StreamingServiceState.PLAYING_STATE;

/**
 *
 */
class StatePlayingSelector implements Func2<MediaPlayer, StreamingServiceState>
{
    private StreamingServiceFSM streamingServiceFSM;

    @Inject
    public StatePlayingSelector(StreamingServiceFSM streamingServiceFSM)
    {
        this.streamingServiceFSM = streamingServiceFSM;
    }

    @Override
    public StreamingServiceState call(@Nonnull MediaPlayer mediaPlayer)
    {
        streamingServiceFSM.setMediaPlayer(mediaPlayer);
        mediaPlayer.start();
        return PLAYING_STATE;
    }
}
