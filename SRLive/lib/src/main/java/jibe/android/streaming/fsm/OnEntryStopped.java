package jibe.android.streaming.fsm;

import com.github.oxo42.stateless4j.delegates.Action;
import com.google.inject.Inject;

/**
 *
 */
class OnEntryStopped implements Action
{
    private StreamingServiceFSM streamingServiceFSM;

    @Inject
    public OnEntryStopped(StreamingServiceFSM streamingServiceFSM)
    {
        this.streamingServiceFSM = streamingServiceFSM;
    }

    @Override
    public void doIt()
    {
        if (streamingServiceFSM.getMediaPlayer().isPresent())
        {
            streamingServiceFSM.getMediaPlayer().get().stop();
        }
    }
}
