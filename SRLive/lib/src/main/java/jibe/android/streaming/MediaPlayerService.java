package jibe.android.streaming;

import android.media.MediaPlayer;
import com.google.inject.Inject;
import jibe.android.service.JibeService;
import jibe.android.streaming.fsm.StreamingServiceFSM;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimeZone;

/**
 *
 */
public class MediaPlayerService extends JibeService implements
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnInfoListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MediaPlayerService.class.getSimpleName());

    @Inject
    private StreamingServiceFSM streamingServiceFSM;

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent)
    {
    }

    @Override
    public void onCompletion(MediaPlayer mp)
    {
        LOGGER.info("onCompletion: " + mp);
        releasePlayer(mp);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        LOGGER.error("onError: " + mp + ", " + what + ", " + extra);
        releasePlayer(mp);
        return true;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra)
    {
        Info event = new Info(what, extra);
        LOGGER.info("onInfo: " + mp + ", " + event.getInfo());

        eventBus.post(event);
        return false;
    }

    private void releasePlayer(MediaPlayer mediaPlayer)
    {
        LOGGER.info("releasePlayer: " + mediaPlayer);
        if (mediaPlayer.isPlaying())
        {
            mediaPlayer.stop();
        }
        mediaPlayer.release();
        eventBus.post(new MediaPlayerReleasedEvent(mediaPlayer));
    }

    public class MediaPlayerReleasedEvent
    {
        private final MediaPlayer mediaPlayer;

        public MediaPlayerReleasedEvent(MediaPlayer mediaPlayer)
        {
            this.mediaPlayer = mediaPlayer;
        }

        public MediaPlayer getMediaPlayer()
        {
            return mediaPlayer;
        }
    }

    public class Info
    {
        private final int extra;

        private final int what;
        private final String ts;

        public Info(int what, int extra)
        {
            this.what = what;
            this.extra = extra;
            this.ts = DateTimeFormat.forPattern("HH:mm:ss").print(new DateTime().withZone(DateTimeZone.forTimeZone(TimeZone.getDefault())));
        }

        public String getInfo()
        {
            return whatToString() + "," + extra + ", " + ts;
        }

        public boolean isBufferingEnd()
        {
            return what == MediaPlayer.MEDIA_INFO_BUFFERING_END;
        }

        public boolean isBufferingStart()
        {
            return what == MediaPlayer.MEDIA_INFO_BUFFERING_START;
        }

        private String whatToString()
        {
            switch (what)
            {
                case MediaPlayer.MEDIA_INFO_UNKNOWN:
                    return "MEDIA_INFO_UNKNOWN";
                case MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING:
                    return "MEDIA_INFO_VIDEO_TRACK_LAGGING";
                case 3: //MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
                    return "MEDIA_INFO_VIDEO_RENDERING_START";
                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                    return "MEDIA_INFO_BUFFERING_START";
                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                    return "MEDIA_INFO_BUFFERING_END";
                case MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING:
                    return "MEDIA_INFO_BAD_INTERLEAVING";
                case MediaPlayer.MEDIA_INFO_NOT_SEEKABLE:
                    return "MEDIA_INFO_NOT_SEEKABLE";
                case MediaPlayer.MEDIA_INFO_METADATA_UPDATE:
                    return "MEDIA_INFO_METADATA_UPDATE";
                case 901: //MediaPlayer.MEDIA_INFO_UNSUPPORTED_SUBTITLE:
                    return "MEDIA_INFO_UNSUPPORTED_SUBTITLE";
                case 902: //MediaPlayer.MEDIA_INFO_SUBTITLE_TIMED_OUT
                    return "MEDIA_INFO_SUBTITLE_TIMED_OUT";
                default:
                    return "UNKNOWN(" + what + ")";
            }
        }
    }
}
