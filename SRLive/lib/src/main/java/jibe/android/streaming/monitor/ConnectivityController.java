package jibe.android.streaming.monitor;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.task.ConnectionSpeedMeterTask;

import java.math.BigDecimal;

/**
 *
 */
public class ConnectivityController
{
    @Inject
    private ApplicationEventBus eventBus;

    @Inject
    private ConnectivityManager connectivityManager;

    public Optional<NetworkInfo> getActiveNetworkInfo()
    {
        return Optional.fromNullable(connectivityManager.getActiveNetworkInfo());
    }

    public Optional<BigDecimal> getConnectivitySpeed()
    {
        if (!haveConnectivity())
        {
            return Optional.absent();
        }

        Optional<ConnectionSpeedMeterTask.Kbps> event = eventBus.getStickyEvent(ConnectionSpeedMeterTask.Kbps.class);
        if (!event.isPresent())
        {
            return Optional.absent();
        }

        BigDecimal value = event.get().getValue();
        if (value.compareTo(BigDecimal.ZERO) == 0)
        {
            return Optional.absent();
        }

        return Optional.of(value);
    }

    public boolean haveConnectivity()
    {
        Optional<NetworkInfo> networkInfo = getActiveNetworkInfo();

        return networkInfo.isPresent() && networkInfo.get().isConnectedOrConnecting();
    }

    public boolean isOnWifi()
    {
        Optional<NetworkInfo> networkInfo = getActiveNetworkInfo();

        return networkInfo.isPresent() && networkInfo.get().getType() == ConnectivityManager.TYPE_WIFI;
    }
}
