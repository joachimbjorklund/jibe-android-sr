package jibe.android.streaming.fsm;

/**
 *
 */
public enum StreamingServiceState
{
    START_STATE,
    PREPARING_STATE,
    PLAYING_STATE,
    PREPARED_STATE,
    PAUSED,
    STOPPED,
    STOPPED_STATE, ERROR_STATE, BUFFERING
}
