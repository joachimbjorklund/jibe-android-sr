package jibe.android.streaming.application;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import com.google.inject.Inject;
import jibe.android.R;
import jibe.android.fragment.BottomFragment;
import jibe.android.fragment.CenterFragment;
import jibe.android.service.HeartBeatService;
import jibe.android.streaming.StreamingService;
import jibe.android.streaming.sr.SRInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public abstract class StreamingActivity extends JibeActivity
{
    private static final Logger LOGGER = LoggerFactory.getLogger(StreamingActivity.class.getSimpleName());

    @Inject
    protected BackPressTwiceToExitHandler backPressTwiceToExitHandler;

    public void createServices()
    {
        super.createServices();

        bindService(StreamingService.class);
        bindService(SRInfoService.class);
        bindService(HeartBeatService.class);
    }

    public BackPressTwiceToExitHandler getBackPressTwiceToExitHandler()
    {
        return backPressTwiceToExitHandler;
    }

    @Override
    public void onBackPressed()
    {
        if (!backPressTwiceToExitHandler.onBackPressed(this))
        {
            return;
        }
        super.onBackPressed();
    }

    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        setContentView(R.layout.main);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        BottomFragment bottomFragment = new BottomFragment();
        fragmentTransaction.add(R.id.bottomLayout, bottomFragment);

        CenterFragment centerFragment = new CenterFragment();
        fragmentTransaction.add(R.id.centerLayout, centerFragment);

        fragmentTransaction.commit();

        eventBus.registerSticky(centerFragment);
        eventBus.registerSticky(bottomFragment);
    }
}
