package jibe.android.streaming.event;

import jibe.android.streaming.application.StreamingActivity;

/**
 *
 */
public class ActivityReadyEvent
{
    private final Class<? extends StreamingActivity> activityClass;
    private final String title;
    private final int iconResourceId;
    private final int channelId;

    public ActivityReadyEvent(Class<? extends StreamingActivity> activityClass, String title, int iconResourceId, int channelId)
    {
        this.activityClass = activityClass;
        this.title = title;
        this.iconResourceId = iconResourceId;
        this.channelId = channelId;
    }

    public Class<? extends StreamingActivity> getActivityClass()
    {
        return activityClass;
    }

    public int getChannelId()
    {
        return channelId;
    }

    public int getIconResourceId()
    {
        return iconResourceId;
    }

    public String getTitle()
    {
        return title;
    }
}
