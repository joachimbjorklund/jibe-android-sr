package jibe.android.streaming.sr;

import com.google.common.base.Optional;
import jibe.android.service.JibeService;
import jibe.android.streaming.event.ActivityReadyEvent;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.event.HeartBeatEvent;
import jibe.sr.api.SRApi;
import jibe.sr.api.json.SRChannel;
import jibe.sr.api.json.SRSchedule;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

/**
 *
 */
public class SRInfoService extends JibeService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SRInfoService.class.getSimpleName());

    private final Set<Integer> registeredChannels = newHashSet();
    private final Map<Integer, SRChannel> trackedChannels = newHashMap();

    public boolean isChannelRegistred(int channelId)
    {
        return registeredChannels.contains(channelId);
    }

    public boolean isChannelTracked(int channelId)
    {
        return trackedChannels.containsKey(channelId);
    }

    @SuppressWarnings("unused")
    public void onEvent(ActivityReadyEvent registerChannel)
    {
        registeredChannels.add(registerChannel.getChannelId());
    }

    @SuppressWarnings("unused")
    public void onEventAsync(HeartBeatEvent event)
    {
        for (int channelId : registeredChannels)
        {
            refreshChannel(channelId);
        }
    }

    private Optional<SRChannel> apiRefreshChannel(final int channelId)
    {
        if (!haveConnectivity())
        {
            LOGGER.warn("No connectivity, SR Api not available right now");
            return Optional.absent();
        }

        return SRApi.refreshChannel(channelId);
    }

    private Optional<SRChannel> getChannelById(final int channelId)
    {
        if (!haveConnectivity())
        {
            LOGGER.warn("No connectivity, SR Api not available right now");
            return Optional.absent();
        }

        Optional<SRChannel> channel;
        try
        {
            channel = SRApi.getChannel(channelId);
        }
        catch (Exception e)
        {
            return Optional.absent();
        }

        return channel;
    }

    private boolean isChannelInfoStale(SRChannel srChannel)
    {
        Optional<SRSchedule> currentScheduledEpisode = srChannel.getCurrentScheduledEpisode();
        if (currentScheduledEpisode.isPresent())
        {
            Optional<DateTime> endTime = currentScheduledEpisode.get().getEndTime();
            return endTime.isPresent() && new DateTime(DateTimeZone.UTC).isAfter(endTime.get());
        }
        return false;
    }

    private void refreshChannel(int channelId)
    {
        Optional<SRChannel> foundChannel = getChannelById(channelId);

        if (!foundChannel.isPresent())
        {
            LOGGER.warn("channel not found: " + channelId);
            return;
        }

        Optional<ChannelInfoEvent> channelInfo = Optional.absent();
        SRChannel srChannel = foundChannel.get();

        if (!trackedChannels.containsKey(channelId))
        {
            trackedChannels.put(channelId, srChannel);
            channelInfo = Optional.of(new ChannelInfoEvent(srChannel));
        }
        else if (isChannelInfoStale(trackedChannels.get(channelId)))
        {
            Optional<SRChannel> refreshedChannel = apiRefreshChannel(channelId);
            if (refreshedChannel.isPresent())
            {
                channelInfo = Optional.of(new ChannelInfoEvent(refreshedChannel.get()));
                trackedChannels.put(channelId, refreshedChannel.get());
            }
        }

        if (channelInfo.isPresent())
        {
            eventBus.postSticky(channelInfo.get());
        }
    }
}
