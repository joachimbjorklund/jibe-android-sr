package jibe.android.streaming.application;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import com.google.inject.Inject;
import jibe.android.service.JibeExecutorService;
import jibe.android.service.JibeService;
import jibe.android.service.LocalJibeServiceConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import roboguice.activity.RoboActivity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class JibeActivity extends RoboActivity
{
    private static final Logger LOGGER = LoggerFactory.getLogger(JibeActivity.class.getSimpleName());

    @Inject
    protected JibeExecutorService executorService;

    @Inject
    protected ApplicationEventBus eventBus;

    @Inject
    private ConnectivityManager connectivityManager;

    private List<LocalJibeServiceConnection> serviceConnections = new ArrayList<>();
    private List<JibeService> createdServices = new ArrayList<>();

    public ApplicationEventBus getEventBus()
    {
        return eventBus;
    }

    public Logger getLogger(Class cls)
    {
        return LoggerFactory.getLogger(cls.getSimpleName());
    }

    public List<LocalJibeServiceConnection> getServiceConnections()
    {
        return serviceConnections;
    }

    public boolean haveConnectivity()
    {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    @Override
    public void onCreate(Bundle sis)
    {
        LOGGER.info("onCreate");
        super.onCreate(sis);

        eventBus.registerSticky(this);

        createServices();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        destroyServices();

        eventBus.removeAllStickyEvents();
        eventBus.unregister(this);
    }

    @SuppressWarnings("unused")
    public void onEvent(ApplicationEventBus.NoOpEvent event)
    {
    }

    protected void createServices()
    {
    }

    protected void bindService(Class<? extends JibeService> service)
    {
        LocalJibeServiceConnection c = new LocalJibeServiceConnection(service);
        if (bindService(new Intent(this, service), c, Context.BIND_AUTO_CREATE))
        {
            serviceConnections.add(c);
        }
    }

    protected void destroyServices()
    {
        for (LocalJibeServiceConnection c : serviceConnections)
        {
            try
            {
                unbindService(c);
            }
            catch (Exception e)
            {
                LOGGER.error("error on unbinding service: " + c.getServiceClass());
            }
        }
    }

//    protected void initStartAppSDK(String appID)
//    {
//        StartAppSDK.init(this, getString(R.string.startapp_dev_id), appID, true);
//    }
//
//    protected void showStartAppSplash(Bundle savedInstanceState)
//    {
//        showStartAppSplash(savedInstanceState, null, null, null);
//    }
//
//    protected void showStartAppSplash(Bundle savedInstanceState, SplashConfig splashConfig)
//    {
//        showStartAppSplash(savedInstanceState, splashConfig, null, null);
//    }
//
//    protected void showStartAppSplash(Bundle savedInstanceState, SplashConfig splashConfig, AdPreferences adPreferences, SplashHideListener splashHideListener)
//    {
//        if (splashConfig == null)
//        {
//            splashConfig = SplashConfig.getDefaultSplashConfig();
//        }
//
//        if (adPreferences == null)
//        {
//            adPreferences = new AdPreferences();
//        }
//
//        if (splashHideListener == null)
//        {
//            splashHideListener = new SplashHideListener()
//            {
//                @Override
//                public void splashHidden()
//                {
//                    LOGGER.info("splashHidden");
//                }
//            };
//        }
//
//        StartAppAd.showSplash(this, savedInstanceState, splashConfig, adPreferences, splashHideListener);
//    }
}
