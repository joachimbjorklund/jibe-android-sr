package jibe.android.streaming;

import android.media.AudioManager;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import jibe.android.streaming.fsm.AudioFocusFSM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;
import static android.media.AudioManager.AUDIOFOCUS_LOSS;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;
import static android.media.AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
import static android.media.AudioManager.STREAM_MUSIC;
import static jibe.android.streaming.fsm.AudioFocusFSM.State.HAVE_AUDIO_FOCUS;
import static jibe.android.streaming.fsm.AudioFocusFSM.Trigger.GAINED;
import static jibe.android.streaming.fsm.AudioFocusFSM.Trigger.LOST_PERMANENT;
import static jibe.android.streaming.fsm.AudioFocusFSM.Trigger.LOST_TRANSIENT;
import static jibe.android.streaming.fsm.AudioFocusFSM.Trigger.LOST_TRANSIENT_CAN_DUCK;

/**
 *
 */
@Singleton
public class AudioFocusController implements AudioManager.OnAudioFocusChangeListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AudioFocusController.class.getSimpleName());

    @Inject
    private AudioManager audioManager;

    @Inject
    private AudioFocusFSM audioFocusFSM;

    public boolean haveAudioFocus()
    {
        return audioFocusFSM.isInState(HAVE_AUDIO_FOCUS);
    }

    @Override
    public void onAudioFocusChange(int focusChange)
    {
        LOGGER.info("onAudioFocusChange: " + audioFocusChangeToName(focusChange));
        switch (focusChange)
        {
            case AUDIOFOCUS_GAIN:
                audioFocusFSM.fire(GAINED);
                break;
            case AUDIOFOCUS_LOSS:
                audioFocusFSM.fire(LOST_PERMANENT);
                break;
            case AUDIOFOCUS_LOSS_TRANSIENT:
                audioFocusFSM.fire(LOST_TRANSIENT);
                break;
            case AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                audioFocusFSM.fire(LOST_TRANSIENT_CAN_DUCK);
                break;
        }
    }

    public boolean requestAudioFocus()
    {
        LOGGER.info("requestAudioFocus");
        AudioFocusFSM.State state = audioFocusFSM.getState();
        if (state == HAVE_AUDIO_FOCUS)
        {
            return true;
        }

        if (audioManager.requestAudioFocus(this, STREAM_MUSIC, AUDIOFOCUS_GAIN) == AUDIOFOCUS_REQUEST_GRANTED)
        {
            audioFocusFSM.fire(GAINED);
        }

        state = audioFocusFSM.getState();
        if (state == HAVE_AUDIO_FOCUS)
        {
            return true;
        }

        return false;
    }

    private String audioFocusChangeToName(int focusChange)
    {
        switch (focusChange)
        {
            case AUDIOFOCUS_GAIN:
                return "AUDIOFOCUS_GAIN";
            case AUDIOFOCUS_LOSS:
                return "AUDIOFOCUS_LOSS";
            case AUDIOFOCUS_LOSS_TRANSIENT:
                return "AUDIOFOCUS_LOSS_TRANSIENT";
            case AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                return "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
            default:
                return "UNKNOWN";
        }
    }
}
