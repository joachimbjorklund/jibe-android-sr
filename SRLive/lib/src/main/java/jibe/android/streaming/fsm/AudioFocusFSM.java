package jibe.android.streaming.fsm;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.delegates.Action;
import com.google.inject.Inject;
import jibe.android.streaming.application.ApplicationEventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static jibe.android.streaming.fsm.AudioFocusFSM.State.DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT;
import static jibe.android.streaming.fsm.AudioFocusFSM.State.DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT_CAN_DUCK;
import static jibe.android.streaming.fsm.AudioFocusFSM.State.HAVE_AUDIO_FOCUS;

/**
 *
 */
public class AudioFocusFSM extends StateMachine<AudioFocusFSM.State, AudioFocusFSM.Trigger>
{
    private final static Logger LOGGER = LoggerFactory.getLogger(AudioFocusFSM.class.getSimpleName());

    @Inject
    private ApplicationEventBus eventBus;

    public AudioFocusFSM()
    {
        super(State.START);

        configure();
    }

    private void configure()
    {
        configure(State.START)
                .permit(Trigger.GAINED, HAVE_AUDIO_FOCUS);

        configure(HAVE_AUDIO_FOCUS)
                .onEntry(new Action()
                {
                    @Override
                    public void doIt()
                    {
                        eventBus.post(HAVE_AUDIO_FOCUS);
                    }
                })
                .permit(Trigger.LOST_PERMANENT, State.DO_NOT_HAVE_AUDIO_FOCUS)
                .permit(Trigger.LOST_TRANSIENT, DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT)
                .permit(Trigger.LOST_TRANSIENT_CAN_DUCK, DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT_CAN_DUCK);

        configure(State.DO_NOT_HAVE_AUDIO_FOCUS)
                .onEntry(new Action()
                {
                    @Override
                    public void doIt()
                    {
                        eventBus.post(State.DO_NOT_HAVE_AUDIO_FOCUS);
                    }
                })
                .permit(Trigger.GAINED, HAVE_AUDIO_FOCUS);

        configure(DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT)
                .onEntry(new Action()
                {
                    @Override
                    public void doIt()
                    {
                        eventBus.post(DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT);
                    }
                })
                .substateOf(State.DO_NOT_HAVE_AUDIO_FOCUS)
                .permit(Trigger.GAINED, HAVE_AUDIO_FOCUS);

        configure(DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT_CAN_DUCK)
                .onEntry(new Action()
                {
                    @Override
                    public void doIt()
                    {
                        eventBus.post(DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT_CAN_DUCK);
                    }
                })
                .substateOf(State.DO_NOT_HAVE_AUDIO_FOCUS)
                .permit(Trigger.GAINED, HAVE_AUDIO_FOCUS);
    }

    public static enum State
    {
        START,
        HAVE_AUDIO_FOCUS,
        DO_NOT_HAVE_AUDIO_FOCUS,

        // substates to DO_NOT_HAVE_AUDIO_FOCUS
        DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT,
        DO_NOT_HAVE_AUDIO_FOCUS_TRANSIENT_CAN_DUCK
    }

    public enum Trigger
    {
        GAINED,
        LOST_PERMANENT,
        LOST_TRANSIENT,
        LOST_TRANSIENT_CAN_DUCK
    }
}
