package jibe.android.streaming.fsm;

import com.github.oxo42.stateless4j.delegates.Func;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import jibe.android.streaming.AudioFocusController;
import jibe.android.streaming.MediaQualityController;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.monitor.ConnectivityController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
@Singleton
class ReadyForPrepareGuard implements Func<Boolean>
{
    private final static Logger LOGGER = LoggerFactory.getLogger(ReadyForPrepareGuard.class.getSimpleName());

    @Inject
    private AudioFocusController audioFocusController;

    @Inject
    private ConnectivityController connectivityController;

    @Inject
    private MediaQualityController mediaQualityController;

    @Inject
    private ApplicationEventBus eventBus;

    @Override
    public Boolean call()
    {
        if (!audioFocusController.haveAudioFocus())
        {
            LOGGER.info("no audio focus, cant start");
            return false;
        }

        if (!connectivityController.haveConnectivity())
        {
            LOGGER.info("no connectivity, cant start");
            return false;
        }

        if (!connectivityController.getConnectivitySpeed().isPresent())
        {
            LOGGER.info("no connectivity speed, cant start");
            return false;
        }

        Optional<ChannelInfoEvent> gotEvent = eventBus.getStickyEvent(ChannelInfoEvent.class);
        if (!gotEvent.isPresent())
        {
            LOGGER.info("no channel info, cant start");
            return false;
        }

        Optional<MediaQualityController.PreferredMedia> preferredMediaQuality = mediaQualityController.getPreferredMediaQuality();
        if (!preferredMediaQuality.isPresent())
        {
            LOGGER.info("no preferred media quality, cant start");
            return false;
        }

        LOGGER.info("all ok for preparing");
        return true;
    }
}
