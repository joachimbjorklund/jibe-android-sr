package jibe.android.streaming.fsm;

import android.media.MediaPlayer;
import com.github.oxo42.stateless4j.triggers.TriggerWithParameters1;
import com.google.inject.Singleton;

/**
 *
 */
@Singleton
public class PreparedTrigger extends TriggerWithParameters1<MediaPlayer, StreamingServiceState, StreamingServiceTrigger>
{
    public PreparedTrigger(StreamingServiceTrigger underlyingTrigger, Class<MediaPlayer> type)
    {
        super(underlyingTrigger, type);
    }
}
