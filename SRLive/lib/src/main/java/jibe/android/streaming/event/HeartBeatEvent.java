package jibe.android.streaming.event;

import org.joda.time.DateTime;

/**
 *
 */
public class HeartBeatEvent
{
    private final DateTime time;

    public HeartBeatEvent()
    {
        this.time = new DateTime();
    }

    public DateTime getCurrentTime()
    {
        return time;
    }
}
