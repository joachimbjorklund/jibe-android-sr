package jibe.android.streaming.event;

import android.net.NetworkInfo;
import com.google.common.base.Optional;

/**
 *
 */
public class NetworkInfoEvent
{
    private final Optional<NetworkInfo> networkInfo;

    public NetworkInfoEvent(Optional<NetworkInfo> networkInfo)
    {
        this.networkInfo = networkInfo;
    }

    public Optional<NetworkInfo> getNetworkInfo()
    {
        return networkInfo;
    }

    public boolean isConnected()
    {
        return networkInfo.isPresent() && networkInfo.get().isConnected();
    }

    public boolean isConnectedOrConnecting()
    {
        return networkInfo.isPresent() && networkInfo.get().isConnectedOrConnecting();
    }
}
