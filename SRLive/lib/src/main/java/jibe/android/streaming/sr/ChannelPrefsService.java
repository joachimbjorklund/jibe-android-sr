package jibe.android.streaming.sr;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import com.google.inject.Inject;
import jibe.android.service.JibeService;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.event.ActivityReadyEvent;
import jibe.android.streaming.event.ChannelColorEvent;
import jibe.android.streaming.event.ChannelImageEvent;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.event.ProgramImageEvent;
import jibe.sr.api.json.SRChannel;
import jibe.sr.api.json.SRProgram;
import jibe.sr.api.json.SRSchedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 *
 */
public class ChannelPrefsService extends JibeService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelPrefsService.class.getSimpleName());

    private static final String PREF_ID = "id";
    private static final String PREF_COLOR = "color";
    private static final String PREF_IMAGE = "image";

    private Optional<SharedPreferences> preferences = Optional.absent();
    private Optional<Integer> channelId = Optional.absent();

    @Inject
    private ApplicationEventBus eventBus;

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @SuppressWarnings("unused")
    public void onEventAsync(ChannelInfoEvent channelInfo)
    {
        SRChannel channel = channelInfo.getChannel();
        boolean updated = Lists.newArrayList(
                commitPref(PREF_ID, channelId),
                commitPref(PREF_COLOR, channel.getColor()),
                commitPref(PREF_IMAGE, channel.getImage()))
                .contains(true);

        if (updated)
        {
            eventBus.postSticky(new ChannelImageEvent(getChannelImage()));
            eventBus.postSticky(new ChannelColorEvent(getChannelColor()));
        }

        eventBus.postSticky(new ProgramImageEvent(saveProgramImage(channel)));
    }

    @SuppressWarnings("unused")
    public void onEventAsync(ActivityReadyEvent event)
    {
        this.preferences = Optional.fromNullable(getSharedPreferences("ChannelInfo-" + event.getChannelId(), Context.MODE_PRIVATE));
        this.channelId = Optional.fromNullable(event.getChannelId());

        eventBus.postSticky(new ChannelImageEvent(getChannelImage()));
        eventBus.postSticky(new ChannelColorEvent(getChannelColor()));
    }

    private Integer assertChannelId()
    {
        if (!channelId.isPresent())
        {
            throw new IllegalStateException("not updated");
        }
        return channelId.get();
    }

    private SharedPreferences assertPreferences()
    {
        if (!preferences.isPresent())
        {
            throw new IllegalStateException("not updated");
        }
        return preferences.get();
    }

    private boolean commitPref(String key, Optional<?> optionalValue)
    {
        SharedPreferences prefs = assertPreferences();

        String value = null;
        if (optionalValue.isPresent())
        {
            value = optionalValue.get().toString();
        }

        String gotValue = prefs.getString(key, null);
        if (prefs.contains(key) && (gotValue != null) && gotValue.equals(value))
        {
            return false;
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();

        return true;
    }

    private Optional<Integer> getChannelColor()
    {
        Optional<String> color = getPreference(PREF_COLOR);
        if (color.isPresent())
        {
            return Optional.of(Color.parseColor("#" + color.get()));
        }
        return Optional.absent();
    }

    private Optional<Uri> getChannelImage()
    {
        File file = getChannelImageFile();

        if (!file.exists())
        {
            Optional<String> prefImage = getPreference(PREF_IMAGE);
            if (!prefImage.isPresent())
            {
                return Optional.absent();
            }
            return saveChannelImage(prefImage.get());
        }
        return Optional.of(Uri.fromFile(file));
    }

    private File getChannelImageFile()
    {
        return new File(getCacheDir(), "channelImage-" + assertChannelId() + ".img");
    }

    private Optional<String> getPreference(String key)
    {
        SharedPreferences prefs = assertPreferences();

        Optional<String> answer = prefs.contains(key) ? Optional.fromNullable(prefs.getString(key, null)) : Optional.<String>absent();
        return answer;
    }

    private File getProgramImageFile(int programId)
    {
        return new File(getCacheDir(), "programImage-" + programId + ".img");
    }

    private Optional<Uri> saveChannelImage(String channelImageURL)
    {
        try
        {
            File file = getChannelImageFile();
            ByteStreams.copy(new URL(channelImageURL).openStream(), new FileOutputStream(file));
            return Optional.of(Uri.fromFile(file));
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage());
        }

        return Optional.absent();
    }

    private Optional<Uri> saveProgramImage(SRChannel channel)
    {
        try
        {
            Optional<SRSchedule> currentScheduledEpisode = channel.getCurrentScheduledEpisode();
            if (!currentScheduledEpisode.isPresent())
            {
                return Optional.absent();
            }

            SRSchedule srSchedule = currentScheduledEpisode.get();
            Optional<SRProgram> program = srSchedule.getProgram();
            if (!program.isPresent())
            {
                return Optional.absent();
            }

            SRProgram srProgram = program.get();
            Optional<URL> programImage = srProgram.getProgramImage();
            if (!programImage.isPresent())
            {
                return Optional.absent();
            }

            URL url = programImage.get();

            File file = getProgramImageFile(srProgram.getId());
            ByteStreams.copy(url.openStream(), new FileOutputStream(file));
            return Optional.of(Uri.fromFile(file));
        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage());
        }

        return Optional.absent();
    }
}
