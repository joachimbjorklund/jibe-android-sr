package jibe.android.streaming;

import android.media.MediaPlayer;
import com.google.inject.Inject;
import jibe.android.streaming.fsm.StreamingServiceFSM;

/**
 *
 */
public class MediaPlayerPreparedListener implements MediaPlayer.OnPreparedListener
{
    @Inject
    private StreamingServiceFSM streamingServiceFSM;

    @Override
    public void onPrepared(MediaPlayer mp)
    {
//        streamingServiceFSM.fire(StreamingServiceTrigger.PREPARED, mp);
    }
}
