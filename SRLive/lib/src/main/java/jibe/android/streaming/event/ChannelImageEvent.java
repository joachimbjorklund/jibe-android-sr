package jibe.android.streaming.event;

import android.net.Uri;
import com.google.common.base.Optional;

/**
 *
 */
public class ChannelImageEvent
{
    private final Optional<Uri> channelImage;

    public ChannelImageEvent(Optional<Uri> channelImage)
    {
        this.channelImage = channelImage;
    }

    public Optional<Uri> getImage()
    {
        return channelImage;
    }
}
