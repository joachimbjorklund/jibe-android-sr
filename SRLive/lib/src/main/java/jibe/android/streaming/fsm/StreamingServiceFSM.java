package jibe.android.streaming.fsm;

import android.content.Context;
import android.media.MediaPlayer;
import com.github.oxo42.stateless4j.delegates.Action2;
import com.github.oxo42.stateless4j.delegates.Func2;
import com.github.oxo42.stateless4j.triggers.TriggerWithParameters1;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import roboguice.RoboGuice;

import static jibe.android.streaming.fsm.StreamingServiceState.ERROR_STATE;
import static jibe.android.streaming.fsm.StreamingServiceState.PLAYING_STATE;
import static jibe.android.streaming.fsm.StreamingServiceState.PREPARED_STATE;
import static jibe.android.streaming.fsm.StreamingServiceState.PREPARING_STATE;
import static jibe.android.streaming.fsm.StreamingServiceState.STOPPED_STATE;
import static jibe.android.streaming.fsm.StreamingServiceTrigger.ERROR;
import static jibe.android.streaming.fsm.StreamingServiceTrigger.STOP_PLAYING;

/**
 *
 */
@Singleton
public class StreamingServiceFSM extends EventBusAwareFSM<StreamingServiceState, StreamingServiceTrigger>
        implements MediaPlayer.OnPreparedListener
{
    private final static Logger LOGGER = LoggerFactory.getLogger(StreamingServiceFSM.class.getSimpleName());

    @Inject
    private ReadyForPrepareGuard readyForPrepareGuard;

    @Inject
    private OnEntryPreparing onEntryPreparing;

    @Inject
    private OnEntryStopped onEntryStopped;

    @Inject
    private PreparedTrigger preparedTrigger;

    @Inject
    private StatePlayingSelector statePlayingSelector;

    private Optional<MediaPlayer> mediaPlayer = Optional.absent();

    private Optional<Exception> exception = Optional.absent();

    @Inject
    public StreamingServiceFSM(Context context)
    {
        super(StreamingServiceState.START_STATE, context);

        RoboGuice.getInjector(context).injectMembersWithoutViews(this);

        configure();
    }

    public Optional<MediaPlayer> getMediaPlayer()
    {
        return mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer)
    {
        this.mediaPlayer = Optional.fromNullable(mediaPlayer);
    }

    @SuppressWarnings("unused")
    public void onEvent(OnEntryPreparing.ExceptionEvent event)
    {
        onException(event.getException());
    }

    @Override // OnPreparedListener
    public void onPrepared(MediaPlayer mediaPlayer)
    {
        fire(preparedTrigger, mediaPlayer);
    }

    private void configure()
    {
        configure(StreamingServiceState.START_STATE)
                .permitIf(StreamingServiceTrigger.PREPARE_TRIGGER, PREPARING_STATE, readyForPrepareGuard);

        configure(PREPARING_STATE)
                .onEntry(onEntryPreparing)
                .permit(StreamingServiceTrigger.PREPARED, StreamingServiceState.PLAYING_STATE)
                .permitDynamic(new TriggerWithParameters1(StreamingServiceTrigger.ERROR, Exception.class), new Func2<Exception, StreamingServiceState>()
                {
                    @Override
                    public StreamingServiceState call(Exception exception)
                    {
                        setException(exception);
                        return ERROR_STATE; //
                    }
                });

        configure(PREPARED_STATE)
                .permitDynamic(preparedTrigger, statePlayingSelector);

        configure(PLAYING_STATE)
                .permit(STOP_PLAYING, StreamingServiceState.STOPPED_STATE);

        configure(STOPPED_STATE)
                .onEntry(onEntryStopped);

        onUnhandledTrigger(new Action2<StreamingServiceState, StreamingServiceTrigger>()
        {
            @Override
            public void doIt(StreamingServiceState state, StreamingServiceTrigger trigger)
            {
                LOGGER.info("unhandled trigger: " + trigger + ", in state: " + state);
            }
        });
    }

    private void fire(StreamingServiceTrigger trigger, Exception exception)
    {
        fire(new TriggerWithParameters1<Exception, StreamingServiceState, StreamingServiceTrigger>(trigger, Exception.class), exception);
    }

    private void onException(Exception exception)
    {
        fire(ERROR, exception);
    }

    private void setException(Exception exception)
    {
        this.exception = Optional.fromNullable(exception);
    }

}
