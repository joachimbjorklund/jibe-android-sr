package jibe.android.streaming.event;

import android.net.Uri;
import com.google.common.base.Optional;

/**
 *
 */
public class ProgramImageEvent
{
    private final Optional<Uri> image;

    public ProgramImageEvent(Optional<Uri> image)
    {
        this.image = image;
    }

    public Optional<Uri> getImage()
    {
        return image;
    }
}
