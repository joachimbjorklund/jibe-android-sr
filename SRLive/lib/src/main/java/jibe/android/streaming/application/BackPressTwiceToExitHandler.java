package jibe.android.streaming.application;

import android.content.Context;
import android.widget.Toast;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import jibe.android.R;
import jibe.android.service.JibeExecutorService;
import org.joda.time.Seconds;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
@Singleton
public class BackPressTwiceToExitHandler
{
    private AtomicInteger backPressed = new AtomicInteger(0);
    private Optional<Toast> oneMoreTimeToExitToast = Optional.absent();

    @Inject
    private JibeExecutorService executorService;

    public int getBackPressed()
    {
        return backPressed.get();
    }

    public boolean onBackPressed(Context context)
    {

        if (backPressed.incrementAndGet() == 1)
        {
            oneMoreTimeToExitToast = Optional.of(Toast.makeText(context, R.string.oneMoreTimeToExit, Toast.LENGTH_SHORT));
            oneMoreTimeToExitToast.get().show();

            executorService.schedule(new Runnable()
            {
                @Override
                public void run()
                {
                    backPressed.decrementAndGet();
                }
            }, Seconds.seconds(4));

            return false;
        }

        if (oneMoreTimeToExitToast.isPresent())
        {
            oneMoreTimeToExitToast.get().cancel();
        }

        return true;
    }
}
