package jibe.android.streaming.event;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import java.util.TimeZone;

/**
 *
 */
public class MediaServiceInfo
{
    private final String info;
    private final String ts;

    public MediaServiceInfo(String info)
    {
        this.info = info;
        this.ts = DateTimeFormat.forPattern("HH:mm:ss").print(new DateTime().withZone(DateTimeZone.forTimeZone(TimeZone.getDefault())));
    }

    public String getInfo()
    {
        return info;
    }
}
