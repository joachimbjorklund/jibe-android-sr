package jibe.android.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.common.base.Optional;
import jibe.android.R;
import jibe.android.streaming.event.ChannelColorEvent;
import jibe.android.streaming.event.ChannelImageEvent;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.event.MediaPlayerStarted;
import jibe.android.streaming.event.NetworkInfoEvent;
import jibe.android.streaming.event.ProgramImageEvent;
import jibe.android.ui.ColorUtil;
import jibe.sr.RightNowProgramInfoBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class CenterFragment extends JibeFragment
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CenterFragment.class.getSimpleName());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View inflate = inflater.inflate(R.layout.center_layout, container, false);
        return inflate;
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ProgramImageEvent event)
    {
        setProgramImage(event.getImage());
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ChannelImageEvent event)
    {
        setChannelImage(event.getImage());
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ChannelColorEvent event)
    {
        if (event.getChannelColor().isPresent())
        {
            setChannelColor(event.getChannelColor().get());
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(MediaPlayerStarted started)
    {
        setMediaQualityText("we have to present to quality here yo!");
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(NetworkInfoEvent event)
    {
        if (!getJibeActivity().haveConnectivity())
        {
            setMediaQualityText("");
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ChannelInfoEvent channelInfo)
    {
        setChannelInfoText(RightNowProgramInfoBuilder.buildRightNowInfoTextCurrent(channelInfo.getChannel(), RightNowProgramInfoBuilder.Format.COMPACT).toString());
    }

    private ImageView findChannelImageView()
    {
        return (ImageView) getActivity().findViewById(R.id.centerLayout_channelImageView);
    }

    private TextView findChannelInfoTextView()
    {
        return (TextView) getActivity().findViewById(R.id.centerLayout_channelInfoTextView);
    }

    private TextView findMediaQualityTextView()
    {
        return (TextView) getActivity().findViewById(R.id.centerLayout_mediaQualityTextView);
    }

    private ImageView findProgramImageView()
    {
        return (ImageView) getActivity().findViewById(R.id.centerLayout_programImageView);
    }

    private void setChannelColor(Integer color)
    {
        int darker = ColorUtil.darker(color);

        findChannelImageView().setBackgroundColor(darker);
        findChannelImageView().setTag(R.id.imageBGColorTagKey, darker); // for testing purposes

        findProgramImageView().setBackgroundColor(darker);
        findProgramImageView().setTag(R.id.imageBGColorTagKey, darker); // for testing purposes
    }

    private void setChannelImage(Optional<Uri> imageUri)
    {
        if (imageUri.isPresent())
        {
            findProgramImageView().setVisibility(View.INVISIBLE);

            findChannelImageView().setTag(R.id.imageUriTagKey, imageUri.get()); // for testing purposes
            findChannelImageView().setImageURI(imageUri.get());
            findChannelImageView().setVisibility(View.VISIBLE);
        }
    }

    private void setChannelInfoText(String text)
    {
        findChannelInfoTextView().setText(text);
    }

    private void setMediaQualityText(String text)
    {
        findMediaQualityTextView().setText(text);
    }

    private void setProgramImage(Optional<Uri> imageUri)
    {
        if (imageUri.isPresent())
        {
            findChannelImageView().setVisibility(View.INVISIBLE);

            findProgramImageView().setTag(R.id.imageUriTagKey, imageUri.get()); // for testing purposes
            findProgramImageView().setImageURI(imageUri.get());
            findProgramImageView().setVisibility(View.VISIBLE);
        }
    }
}
