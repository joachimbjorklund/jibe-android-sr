package jibe.android.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.common.base.Optional;
import com.google.inject.Inject;
import jibe.android.R;
import jibe.android.streaming.MediaPlayerPauseRequest;
import jibe.android.streaming.MediaPlayerService;
import jibe.android.streaming.MediaPlayerStartRequest;
import jibe.android.streaming.event.ChannelColorEvent;
import jibe.android.streaming.event.ChannelImageEvent;
import jibe.android.streaming.event.ChannelInfoEvent;
import jibe.android.streaming.event.MediaPlayerPaused;
import jibe.android.streaming.event.MediaPlayerStarted;
import jibe.android.streaming.event.ProgramImageEvent;
import jibe.android.streaming.fsm.StreamingServiceFSM;
import jibe.sr.RightNowProgramInfoBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import roboguice.inject.InjectView;

/**
 *
 */
public class BottomFragment extends JibeFragment
{
    private static final Logger LOGGER = LoggerFactory.getLogger(BottomFragment.class.getSimpleName());

    @InjectView(tag = "bottomLayout_textView")
    private TextView textView;

    @Inject
    private StreamingServiceFSM streamingServiceFSM;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.bottom_layout, container, false);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ChannelInfoEvent channelInfo)
    {
        Optional<String> text = Optional.of(
                RightNowProgramInfoBuilder.buildRightNowInfoText(channelInfo.getChannel(),
                        RightNowProgramInfoBuilder.Format.FULL)
        );
        setChannelInfoText(text);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(MediaPlayerService.MediaPlayerReleasedEvent mediaPlayerReleasedEvent)
    {
        toggleMediaButtonPreparing();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(MediaPlayerPaused paused)
    {
        toggleMediaButtonPaused();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(MediaPlayerStarted started)
    {
        toggleMediaButtonStarted();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ChannelColorEvent event)
    {
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(MediaPlayerService.Info event)
    {
        if (event.isBufferingStart())
        {
            toggleMediaButtonBufferingStart();
        }
        else if (event.isBufferingEnd())
        {
            toggleMediaButtonBufferingEnd();
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ChannelImageEvent event)
    {
        if (event.getImage().isPresent())
        {
            setImage(event.getImage().get());
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ProgramImageEvent event)
    {
        if (event.getImage().isPresent())
        {
            setImage(event.getImage().get());
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        textView.setSelected(true);
        setupStartButton();
        setupStopButton();
    }

    private ImageView findImageView()
    {
        return (ImageView) getActivity().findViewById(R.id.bottomLayout_imageView);
    }

    private ProgressBar findProgressBar()
    {
        return (ProgressBar) getActivity().findViewById(R.id.bottomLayout_progressBar);
    }

    private Button findStartButton()
    {
        return (Button) getActivity().findViewById(R.id.bottomLayout_startButton);
    }

    private Button findStopButton()
    {
        return (Button) getActivity().findViewById(R.id.bottomLayout_stopButton);
    }

    private void saveCurrentAndSetChannelInfoText(Optional<String> text)
    {
        setChannelInfoText(text);
    }

    private void setChannelInfoText(Optional<String> text)
    {
        if (text.isPresent())
        {
            textView.setText(text.get());
        }
        else
        {
            textView.setText("");
        }
    }

    private void setImage(Uri imageUri)
    {
        findImageView().setImageURI(imageUri);
        findImageView().setTag(R.id.imageUriTagKey, imageUri); // for testing purposes
    }

    private void setupStartButton()
    {
        findStartButton().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getJibeActivity().getEventBus().post(new MediaPlayerStartRequest());
            }
        });
    }

    private void setupStopButton()
    {
        findStopButton().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getJibeActivity().getEventBus().post(new MediaPlayerPauseRequest());
            }
        });
    }

    private void spinnerOff()
    {
        findProgressBar().setVisibility(View.INVISIBLE);
    }

    private void spinnerOn()
    {
        findProgressBar().setVisibility(View.VISIBLE);
    }

    private void toggleMediaButtonBufferingEnd()
    {
        toggleMediaButtonStarted();
    }

    private void toggleMediaButtonBufferingStart()
    {
        toggleMediaButtonCompleted();
    }

    private void toggleMediaButtonCompleted()
    {
        findStartButton().setVisibility(View.INVISIBLE);
        findStopButton().setVisibility(View.INVISIBLE);
        spinnerOn();
    }

    private void toggleMediaButtonPaused()
    {
        findStartButton().setVisibility(View.VISIBLE);
        findStopButton().setVisibility(View.INVISIBLE);
        spinnerOff();
    }

    private void toggleMediaButtonPreparing()
    {
        toggleMediaButtonCompleted();

        saveCurrentAndSetChannelInfoText(Optional.of(getString(R.string.preparing)));
    }

    private void toggleMediaButtonStarted()
    {
        findStartButton().setVisibility(View.INVISIBLE);
        findStopButton().setVisibility(View.VISIBLE);
        spinnerOff();
    }
}
