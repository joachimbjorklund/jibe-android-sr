package jibe.android.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import jibe.android.R;

/**
 *
 */
public class MediaQualityPreferenceFragment extends PreferenceFragment
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.media_quality_preference);
    }
}
