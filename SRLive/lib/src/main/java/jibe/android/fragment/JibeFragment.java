package jibe.android.fragment;

import android.os.Bundle;
import com.google.inject.Inject;
import jibe.android.streaming.application.ApplicationEventBus;
import jibe.android.streaming.application.JibeActivity;
import roboguice.fragment.provided.RoboFragment;

/**
 *
 */
public class JibeFragment extends RoboFragment
{
    @Inject
    protected ApplicationEventBus eventBus;

    public JibeActivity getJibeActivity()
    {
        return (JibeActivity) getActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        eventBus.registerSticky(this);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        eventBus.unregister(this);
    }

    @SuppressWarnings("unused")
    public void onEvent(ApplicationEventBus.NoOpEvent event)
    {
    }
}
