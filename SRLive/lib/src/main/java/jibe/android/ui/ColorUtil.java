package jibe.android.ui;

import android.graphics.Color;

/**
 *
 */
public class ColorUtil
{
    private ColorUtil()
    {

    }

    public static int darker(int color)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f;
        return Color.HSVToColor(hsv);
    }

    public static int lighter(int color)
    {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] = 1.0f - 0.8f * (1.0f - hsv[2]);
        return Color.HSVToColor(hsv);
    }
}
