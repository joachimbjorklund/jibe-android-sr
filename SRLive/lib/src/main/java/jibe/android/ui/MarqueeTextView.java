package jibe.android.ui;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
@SuppressWarnings("unused")
public class MarqueeTextView extends TextView
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MarqueeTextView.class.getSimpleName());

    public MarqueeTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public MarqueeTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public MarqueeTextView(Context context)
    {
        super(context);
    }

    @Override
    public boolean isFocused()
    {
        return true;
    }

    @Override
    public void onWindowFocusChanged(boolean focused)
    {
        if (focused)
        {
            super.onWindowFocusChanged(focused);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        LOGGER.info("onSizeChanged");
        super.onSizeChanged(w, h, oldw, oldh);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
        params.width = w;
        params.height = h;
        params.gravity = Gravity.CENTER_HORIZONTAL;
//        params.weight = 0;
        setLayoutParams(params);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect)
    {
        if (focused)
        {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
        }
    }
}
