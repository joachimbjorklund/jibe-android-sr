package jibe.p3.star;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class P3StarActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 1607;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(P3StarActivity.class, appName, R.drawable.p3_star_512_512, SR_API_CHANNEL));
    }
}
