package jibe.p1;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class P1Activity extends StreamingActivity
{
    public static final int SR_API_CHANNEL = 132;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(P1Activity.class, appName, R.drawable.p1_512_512, SR_API_CHANNEL));
    }
}