package jibe.p2.klassiskt;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;


public class P2KlassisktActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 1603;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);

        eventBus.postSticky(new ActivityReadyEvent(P2KlassisktActivity.class, appName, R.drawable.p2_klassiskt_512_512, SR_API_CHANNEL));
    }
}
