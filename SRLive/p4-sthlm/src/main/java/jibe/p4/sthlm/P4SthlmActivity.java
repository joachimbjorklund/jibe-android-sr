package jibe.p4.sthlm;

import android.os.Bundle;
import jibe.android.streaming.application.StreamingActivity;
import jibe.android.streaming.event.ActivityReadyEvent;
import roboguice.inject.InjectResource;

public class P4SthlmActivity extends StreamingActivity
{
    private static final int SR_API_CHANNEL = 701;

    @InjectResource(R.string.app_name)
    private String appName;

    @Override
    public void onCreate(Bundle sis)
    {
        super.onCreate(sis);
        eventBus.postSticky(new ActivityReadyEvent(P4SthlmActivity.class, appName, R.drawable.p4_sthlm_512_512, SR_API_CHANNEL));
    }
}
